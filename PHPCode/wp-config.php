<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mombo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p=Bkk;;t}CI4B}xo^ns?l#1]k/@S6LW-V}tS>6SHk[QQ=g#ZL~WLszfw=XJ__(TS');
define('SECURE_AUTH_KEY',  'FZjgDeWR7_ hAuXMy4[-yhUw7{is~HpAA6;i$>oj1v~bXwb6^zRl:JQMRxXRI4>n');
define('LOGGED_IN_KEY',    'A#<0|>_m:6 jk8ynghhcM#sK+IMdEq7xj [*Jafn`o8ur+cX~6pcGN<O2|~?FH^U');
define('NONCE_KEY',        '0)/&Qz]Oh%SC;+Co5TJbNV`E=wUp~>C5h%zA,/-r3/(?7g%f2C)RB$Y8Ik),jT5R');
define('AUTH_SALT',        'MzQJ(BPp<#~NFKo/k[H?+X`A1DuG;F7*>DZl)JFPT{#<KsfMHz_*O){4N>}Rx`EE');
define('SECURE_AUTH_SALT', '(7ss~Hk7Q}NU.%axE]er#-<169bB9<Ge&{&Q}WR}Wt:H7?GPf99mbiWiYm^`DE!&');
define('LOGGED_IN_SALT',   'Up3XKQq9E{t6Cnvj9z?Ag:?[=0Rwh]5I?,m]yP5tsue/eO-4n|dy875CW_d~<7@T');
define('NONCE_SALT',       'Bb)LxwQr,T0~HrJ2kBT~;)tfo^>fzu!uPOKnPePsVw7p*1x-gV4(88GK71@V@|UA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');