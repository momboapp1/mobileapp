<?php 
require "/home/html/mombo/africatalking/vendor/autoload.php";
use AfricasTalking\SDK\AfricasTalking;

require "/home/html/mombo/infobip/vendor/autoload.php";

/*
Controller name: Core
Controller description: Basic introspection methods
*/
class JSON_API_Core_Controller {
  
  public function info() {
    global $json_api;
    $php = '';
    if (!empty($json_api->query->controller)) {
      return $json_api->controller_info($json_api->query->controller);
    } else {
      $dir = json_api_dir();
      if (file_exists("$dir/json-api.php")) {
        $php = file_get_contents("$dir/json-api.php");
      } else {
        // Check one directory up, in case json-api.php was moved
        $dir = dirname($dir);
        if (file_exists("$dir/json-api.php")) {
          $php = file_get_contents("$dir/json-api.php");
        }
      }
      if (preg_match('/^\s*Version:\s*(.+)$/m', $php, $matches)) {
        $version = $matches[1];
      } else {
        $version = '(Unknown)';
      }
      $active_controllers = explode(',', get_option('json_api_controllers', 'core'));
      $controllers = array_intersect($json_api->get_controllers(), $active_controllers);
      return array(
        'json_api_version' => $version,
        'controllers' => array_values($controllers)
      );
    }
  }
  
  public function get_recent_posts() {
    global $json_api;
    $posts = $json_api->introspector->get_posts();
    return $this->posts_result($posts);
  }
  
  public function get_posts() {
    global $json_api;
    $url = parse_url($_SERVER['REQUEST_URI']);
    $defaults = array(
      'ignore_sticky_posts' => true
    );
    $query = wp_parse_args($url['query']);
    unset($query['json']);
    unset($query['post_status']);
    $query = array_merge($defaults, $query);
    $posts = $json_api->introspector->get_posts($query);
    $result = $this->posts_result($posts);
    $result['query'] = $query;
    return $result;
  }
  
  public function get_post() {
    global $json_api, $post;
    $post = $json_api->introspector->get_current_post();
    if ($post) {
      $previous = get_adjacent_post(false, '', true);
      $next = get_adjacent_post(false, '', false);
      $response = array(
        'post' => new JSON_API_Post($post)
      );
      if ($previous) {
        $response['previous_url'] = get_permalink($previous->ID);
      }
      if ($next) {
        $response['next_url'] = get_permalink($next->ID);
      }
      return $response;
    } else {
      $json_api->error("Not found.");
    }
  }

  public function get_page() {
    global $json_api;
    extract($json_api->query->get(array('id', 'slug', 'page_id', 'page_slug', 'children')));
    if ($id || $page_id) {
      if (!$id) {
        $id = $page_id;
      }
      $posts = $json_api->introspector->get_posts(array(
        'page_id' => $id
      ));
    } else if ($slug || $page_slug) {
      if (!$slug) {
        $slug = $page_slug;
      }
      $posts = $json_api->introspector->get_posts(array(
        'pagename' => $slug
      ));
    } else {
      $json_api->error("Include 'id' or 'slug' var in your request.");
    }
    
    // Workaround for https://core.trac.wordpress.org/ticket/12647
    if (empty($posts)) {
      $url = $_SERVER['REQUEST_URI'];
      $parsed_url = parse_url($url);
      $path = $parsed_url['path'];
      if (preg_match('#^http://[^/]+(/.+)$#', get_bloginfo('url'), $matches)) {
        $blog_root = $matches[1];
        $path = preg_replace("#^$blog_root#", '', $path);
      }
      if (substr($path, 0, 1) == '/') {
        $path = substr($path, 1);
      }
      $posts = $json_api->introspector->get_posts(array('pagename' => $path));
    }
    
    if (count($posts) == 1) {
      if (!empty($children)) {
        $json_api->introspector->attach_child_posts($posts[0]);
      }
      return array(
        'page' => $posts[0]
      );
    } else {
      $json_api->error("Not found.");
    }
  }
  
  public function get_date_posts() {
    global $json_api;
    if ($json_api->query->date) {
      $date = preg_replace('/\D/', '', $json_api->query->date);
      if (!preg_match('/^\d{4}(\d{2})?(\d{2})?$/', $date)) {
        $json_api->error("Specify a date var in one of 'YYYY' or 'YYYY-MM' or 'YYYY-MM-DD' formats.");
      }
      $request = array('year' => substr($date, 0, 4));
      if (strlen($date) > 4) {
        $request['monthnum'] = (int) substr($date, 4, 2);
      }
      if (strlen($date) > 6) {
        $request['day'] = (int) substr($date, 6, 2);
      }
      $posts = $json_api->introspector->get_posts($request);
    } else {
      $json_api->error("Include 'date' var in your request.");
    }
    return $this->posts_result($posts);
  }
  
  public function get_category_posts() {
    global $json_api;
    $category = $json_api->introspector->get_current_category();
    if (!$category) {
      $json_api->error("Not found.");
    }
    $posts = $json_api->introspector->get_posts(array(
      'cat' => $category->id
    ));
    return $this->posts_object_result($posts, $category);
  }
  
  public function get_tag_posts() {
    global $json_api;
    $tag = $json_api->introspector->get_current_tag();
    if (!$tag) {
      $json_api->error("Not found.");
    }
    $posts = $json_api->introspector->get_posts(array(
      'tag' => $tag->slug
    ));
    return $this->posts_object_result($posts, $tag);
  }
  
  public function get_author_posts() {
    global $json_api;
    $author = $json_api->introspector->get_current_author();
    if (!$author) {
      $json_api->error("Not found.");
    }
    $posts = $json_api->introspector->get_posts(array(
      'author' => $author->id
    ));
    return $this->posts_object_result($posts, $author);
  }
  
  public function get_search_results() {
    global $json_api;
    if ($json_api->query->search) {
      $posts = $json_api->introspector->get_posts(array(
        's' => $json_api->query->search
      ));
    } else {
      $json_api->error("Include 'search' var in your request.");
    }
    return $this->posts_result($posts);
  }
  
  public function get_date_index() {
    global $json_api;
    $permalinks = $json_api->introspector->get_date_archive_permalinks();
    $tree = $json_api->introspector->get_date_archive_tree($permalinks);
    return array(
      'permalinks' => $permalinks,
      'tree' => $tree
    );
  }
  
  public function get_category_index() {
    global $json_api;
    $args = null;
    if (!empty($json_api->query->parent)) {
      $args = array(
        'parent' => $json_api->query->parent
      );
    }
    $categories = $json_api->introspector->get_categories($args);
    return array(
      'count' => count($categories),
      'categories' => $categories
    );
  }
  
  public function get_tag_index() {
    global $json_api;
    $tags = $json_api->introspector->get_tags();
    return array(
      'count' => count($tags),
      'tags' => $tags
    );
  }
  
  public function get_author_index() {
    global $json_api;
    $authors = $json_api->introspector->get_authors();
    return array(
      'count' => count($authors),
      'authors' => array_values($authors)
    );
  }
  
  public function get_page_index() {
    global $json_api;
    $pages = array();
    $post_type = $json_api->query->post_type ? $json_api->query->post_type : 'page';
    
    // Thanks to blinder for the fix!
    $numberposts = empty($json_api->query->count) ? -1 : $json_api->query->count;
    $wp_posts = get_posts(array(
      'post_type' => $post_type,
      'post_parent' => 0,
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'numberposts' => $numberposts
    ));
    foreach ($wp_posts as $wp_post) {
      $pages[] = new JSON_API_Post($wp_post);
    }
    foreach ($pages as $page) {
      $json_api->introspector->attach_child_posts($page);
    }
    return array(
      'pages' => $pages
    );
  }
  
  public function get_nonce() {
    global $json_api;
    extract($json_api->query->get(array('controller', 'method')));
    if ($controller && $method) {
      $controller = strtolower($controller);
      if (!in_array($controller, $json_api->get_controllers())) {
        $json_api->error("Unknown controller '$controller'.");
      }
      require_once $json_api->controller_path($controller);
      if (!method_exists($json_api->controller_class($controller), $method)) {
        $json_api->error("Unknown method '$method'.");
      }
      $nonce_id = $json_api->get_nonce_id($controller, $method);
      return array(
        'controller' => $controller,
        'method' => $method,
        'nonce' => wp_create_nonce($nonce_id)
      );
    } else {
      $json_api->error("Include 'controller' and 'method' vars in your request.");
    }
  }
  
  protected function get_object_posts($object, $id_var, $slug_var) {
    global $json_api;
    $object_id = "{$type}_id";
    $object_slug = "{$type}_slug";
    extract($json_api->query->get(array('id', 'slug', $object_id, $object_slug)));
    if ($id || $$object_id) {
      if (!$id) {
        $id = $$object_id;
      }
      $posts = $json_api->introspector->get_posts(array(
        $id_var => $id
      ));
    } else if ($slug || $$object_slug) {
      if (!$slug) {
        $slug = $$object_slug;
      }
      $posts = $json_api->introspector->get_posts(array(
        $slug_var => $slug
      ));
    } else {
      $json_api->error("No $type specified. Include 'id' or 'slug' var in your request.");
    }
    return $posts;
  }
  
  protected function posts_result($posts) {
    global $wp_query;
    return array(
      'count' => count($posts),
      'count_total' => (int) $wp_query->found_posts,
      'pages' => $wp_query->max_num_pages,
      'posts' => $posts
    );
  }
  
  protected function posts_object_result($posts, $object) {
    global $wp_query;
    // Convert something like "JSON_API_Category" into "category"
    $object_key = strtolower(substr(get_class($object), 9));
    return array(
      'count' => count($posts),
      'pages' => (int) $wp_query->max_num_pages,
      $object_key => $object,
      'posts' => $posts
    );
  }
  	public function _requestStatus($code) {
		$status = array(
					1012 => 'Username already exists',
					2006 => 'User id must not be blank',
					2001 => 'Verification code not correct!',
					2002 => 'Email Verification code not correct!',
					2003 => 'Phone Verification code not correct!',
					2004 => 'Verification code correct!',
					2023 => 'Login pin not verified!',
					2024 => 'Login pin verified!',
					2025 => 'Secret word not correct!',
					2026 => 'Pin does not match!',
					2027 => 'Login pin reset successfully!',
					2028 => 'Age is less than 18 year',
					2029 => 'National ID/Passport number exists',
					2030 => 'Mobile number already exists',
					2041 => 'Terms and conditions fetched successfully',
					2042 => 'Secret word reset successfully!',
					2043 => 'Secret word correct!',
					1004 => 'SignUp Successfully',
					1009 => 'Email already exists',
					1001 => 'Login Successfully',
					1005 => 'Please Try Again',
					7002 => 'Please fill all required fields',
					1006 => "Email doesn't exists",
					1011 => 'Mail Sent Successfully',
					3001 => 'Products fetched Successfully',
					7012 => 'Country List Fetched Successfully',
					7013 => 'Country calling code fetched successfully',
					10002 => 'Phone verification code does not create',
					10001 => 'Phone verification code created',
					10003 => 'Email verification code created',
					10002 => 'Email verification code does not create',
				  );
		return $status[$code];
	}
	
	
	public function signup()
	{  
		global $json_api ,$server_path,$wpdb;
		$json           = file_get_contents("php://input");
		$data           = json_decode($json);
        // $user_id          = $data->user_id;		
		// $gender         = $data->gender;
		// $country_calling_code   = $data->country_calling_code;
		$country       = $data->country; 
		$country_code       = $data->country_code; 
		$national_id       = $data->national_id; 
		$dob           = $data->dob; 
		$dob_n = str_replace('/', '-', $dob);
		$dateofbirth =  date('Y',strtotime($dob_n));
		$current =  date("Y");
		$difference = $current - $dateofbirth;
		//echo $difference; die;
		if($difference <= 18)
		{
			$message = $this->_requestStatus("2028");
			return array(
				"RespCode" => "2028", //'Age is less than 18 year',
				"success" => "false",
				"Message" => $message
			);
		} 
		$mobile_number  = $data->mobile_number;		
		$email  = $data->email;
		$ar = explode('@',$email);
		$rand = rand(11111,999999);
		$username = $ar[0].$rand;
		// $status  = $data->status;
		// $secret_message  = $data->secret_message;
		// $password  = $data->password;
		// $devicetype = $data->devicetype;
		// $devicetoken = $data->deviceToken;
		$deviceId = $data->deviceId;
		/* $userData = get_userdata( $user_id );
		$dattime = $userData->data->user_registered;
		$date = date('Y-m-d h:i:s',strtotime($dattime)); */

		// $user_id = username_exists($username);
		// if ($user_id){
			// $message = $this->_requestStatus("1012");
			// return array(
				// "RespCode" => "1012", //1012 => 'Username already exists',
				// "success" => "false",
				// "Message" => $message
			// );
		// }
				
		$nationalid_exist = array(				
				'meta_key'     => 'national_id',
				'meta_value'   => $national_id,
				'meta_compare' => 'EXISTS',				
			 ); 
	    if(get_users($nationalid_exist))
		{
			$message = $this->_requestStatus("2029");
			return array(
				"RespCode" => "2029", //2029 => 'National ID/ Passport number exists',
				"success" => "false",
				"Message" => $message
			); 
		}
		
		$mobileno_exist = array(				
				'meta_key'     => 'mobile_number',
				'meta_value'   => $mobile_number,
				'meta_compare' => 'EXISTS',				
			 ); 
	    if(get_users($mobileno_exist))
		{
			$message = $this->_requestStatus("2030");
			return array(
				"RespCode" => "2030", //2030 => 'Mobile number already exists',
				"success" => "false",
				"Message" => $message
			); 
		}
		
		if (email_exists($email) == false) {           		
			/* $user_id = wp_create_user( $username, $password, $email);			
			update_user_meta( $user_id, 'gender', $gender );
			//update_user_meta( $user_id, 'country_flag', $country_flag );
			update_user_meta( $user_id, 'country', $country );
			update_user_meta( $user_id, 'country_calling_code', $country_calling_code );
			update_user_meta( $user_id, 'national_id', $national_id );
			update_user_meta( $user_id, 'dob', $dob );
			update_user_meta( $user_id, 'user_login', $username );
			update_user_meta( $user_id, 'mobile_number', $mobile_number );
			update_user_meta( $user_id, 'user_email', $email );
			update_user_meta( $user_id, 'status', $status );
			update_user_meta( $user_id, 'secret_message', $secret_message );
			update_user_meta( $user_id,'devicetoken',$devicetoken);
			update_user_meta( $user_id,'devicetype',$devicetype);  			 */			
			
			// if($user_id == ""){
				// $message = $this->_requestStatus("2006"); 
				// return array(
					// "RespCode" => "2006", //2006 => 'User id must not be blank',
					// "success" => "false", 
					// "Message" => $message
				// );
			// } else { 

			// }			
				/* $user_data = array("id" => $user_id,
				    "user_login" => $username,
					"user_email" => $email,
					"gender" => $gender,
					// "country_flag" => $country_flag,
					"country_calling_code" => $country_calling_code,
					"national_id" => $national_id,
					"mobile_number" => $mobile_number,
					"dob" => $dob,
					"country" => $country,
					"status" => 0,
					"secret_message" => $secret_message					
				);	
				$msg = 'SignUp successfully'; */
				
			$apiurl = 'https://restcountries.eu/rest/v1/alpha?codes='.$country_code;
			$idd=json_decode(file_get_contents($apiurl)); 
			$country_calling_code = $idd[0]->callingCodes[0];
			$letters='abcdefghijklmnopqrstuvwxyz';  // selection of a-z
			$otp='';  // declare empty string
			for($x=0; $x<2; ++$x){  
				$otp.=$letters[rand(0,25)].rand(0,9);  
			}		
			//Africa talking send sms code
		 	 $username = 'sandbox'; // use 'sandbox' for development in the test environment
			$apiKey   = 'aaf97a8b6fc4e1dbf8d5d309c209ad3b1847a523f231013744017d08bd674c82'; // use your sandbox app API key for development in the test environment
			$AT       = new AfricasTalking($username, $apiKey);
			$sms      = $AT->sms();

			// Use the service
			$result   = $sms->send([
			  //'to'      => array('+918054578142'),
				'to'      => '+'.$country_calling_code.$mobile_number,
				'message' =>  'Your Phone verification Code is '.$otp.'',
			]);  
			//Infobip send sms code
		   /*  $client = new infobip\api\client\SendSingleTextualSms(new infobip\api\configuration\BasicAuthConfiguration('Kaur', '@password1'));
			$requestBody = new infobip\api\model\sms\mt\send\textual\SMSTextualRequest();
			$requestBody->setFrom('+'.$country_calling_code.$mobile_number);
			$requestBody->setTo('+'.$country_calling_code.$mobile_number);
			$requestBody->setText('Your Phone verification Code is '.$otp.'');
			$response = $client->execute($requestBody);
			print_r($response); die();  */

			//update_user_meta($user_id,'phone_verification_code',$otp); 
			$opt_name_email = $deviceId.'_email';
			$opt_name_phone = $deviceId.'_phone';
			
			
			$letters='abcdefghijklmnopqrstuvwxyz';  // selection of a-z
			$emailcode='';  // declare empty string
			for($x=0; $x<3; ++$x){  // loop three times
				$emailcode.=$letters[rand(0,25)].rand(0,9);  // concatenate one letter then one number
			}
			$message = "<p>Dear ".$ar[0]." <br><br>";
			$message .= "Your email verification Code is ".$emailcode."";
			$message .= "<br> Your Phone verification Code is ".$otp."";
			$message .= '<p>Thank You<br> 
			The Mombo Team</p> <br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/locationIcon.png" style="width:14px;"> Landmark Plaza, Argwings Kodhek Road, Nairobi<br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/phoneIcon.png" style="width:14px;"> +254 706 503 230 <br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/emailIcon.png" style="width:14px;"> support@mombo.africa <br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/urlIcon.png" style="width:14px;"> www.mombo.co.ke';
			$to = $email;
			$subject="Email verification code";
			$headers[] = 'Content-Type: text/html; charset=UTF-8';
			$headers[] = 'From: Mombo <noreply@getyoursolutions.com>';
			if(wp_mail($to,$subject,$message,$headers))
			{
				 update_option($opt_name_email, $emailcode );
			     update_option($opt_name_phone, $otp );
				//update_user_meta($user_id,'email_verification_code',$emailcode);
				//update_user_meta($user_id,'phone_verification_code',$otp);
				$msg .= 'Email verification code sent';
			}else{
				$msg .= 'Email verification code not sent';
			}

			return array(
					"RespCode" => "1004", //1004 => 'SignUp Successfully',
					"success" => "true", 
					"Message" => $msg,
					// "user_data" => $user_data,
			  );									
			// }
		}else{
			$message = $this->_requestStatus("1009");
			return array(
				"RespCode" => "1009", //1009 => 'Email already exists'
				"success" => "false",
				"Message" => $message,
			);
		}
	}
	
	public function code_verification()
	{
		global $json_api,$wpdb;
		$json     = file_get_contents("php://input");
		$data     = json_decode($json);
		$user_id = $data->user_id;
		$gender  = $data->gender;
		$country  = $data->country; 
		$country_code  = $data->country_code; 
		$national_id  = $data->national_id; 
		$dob   = $data->dob; 
		$mobile_number  = $data->mobile_number;	
		$status  = $data->status;
	    $secret_message  = $data->secret_message;
		$password  = $data->password;
		$email  = $data->email;
		$ar = explode('@',$email);
		$rand = rand(11111,999999);
		$username = $ar[0].$rand;
		$status  = $data->status;
		$password  = $data->password;
		$phone_code = $data->phone_code;
		$email_code = $data->email_code;
		$deviceId = $data->deviceId;
		$opt_name_email = $deviceId.'_email';
		$opt_name_phone = $deviceId.'_phone';
		// $phone_verification_code = get_user_meta($user_id,'phone_verification_code',true);
		$phone_verification_code = get_option($opt_name_phone);
		// $email_verification_code = get_user_meta($user_id,'email_verification_code',true);
		$email_verification_code = get_option($opt_name_email);
		if($phone_code != $phone_verification_code && $email_code != $email_verification_code)
		{
			$message = $this->_requestStatus("2001");
			return array(
				"RespCode" => "2001", // Verification code not correct!
				"success" => "false",
				"Message" => $message,
			);
		}else if($phone_code == $phone_verification_code && $email_code != $email_verification_code)
		{			
			$message = $this->_requestStatus("2002");
			return array(
				"RespCode" => "2002", //Email Verification code not correct!
				"success" => "false",
				"Message" => $message,
			);
		}else if($phone_code != $phone_verification_code && $email_code == $email_verification_code)
		{			
			$message = $this->_requestStatus("2003");
			return array(
				"RespCode" => "2003", // Phone Verification code not correct!
				"success" => "false",
				"Message" => $message,
			);
		}else if($phone_code == $phone_verification_code && $email_code == $email_verification_code)
		{
			//User create - start
			$user_id = wp_create_user( $username, $password, $email);			
			update_user_meta( $user_id, 'gender', $gender );
			//update_user_meta( $user_id, 'country_flag', $country_flag );
			update_user_meta( $user_id, 'country', $country );
			update_user_meta( $user_id, 'country_code', $country_code );
			update_user_meta( $user_id, 'national_id', $national_id );
			update_user_meta( $user_id, 'dob', $dob );
			update_user_meta( $user_id, 'user_login', $username );
			update_user_meta( $user_id, 'mobile_number', $mobile_number );
			update_user_meta( $user_id, 'user_email', $email );
			update_user_meta( $user_id, 'status', $status );
			update_user_meta( $user_id, 'secret_message', $secret_message );
			update_user_meta( $user_id,'devicetoken',$devicetoken);
			update_user_meta( $user_id,'devicetype',$devicetype);  	
			update_user_meta( $user_id,'deviceId',$deviceId);  	
			$user_data = array("id" => $user_id,
				    "user_login" => $username,
					"user_email" => $email,
					"gender" => $gender,
					// "country_flag" => $country_flag,
					"country_calling_code" => $country_calling_code,
					"national_id" => $national_id,
					"mobile_number" => $mobile_number,
					"dob" => $dob,
					"country" => $country,
					"country_code" => $country_code,
					"status" => 0,
					"secret_message" => $secret_message					
				);		
			delete_option($opt_name_phone);
			delete_option($opt_name_email);
			//User create - end
			$msg = '';
			$digits = 4;
			$pin_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
			$message = "<p>Dear ".$ar[0]." <br><br>";
			$message .= "Use this PIN ".$pin_number." to login in to your Mombo account. Once logged in, change your PIN.";
			$message .= '<p>Thank You<br> 
			The Mombo Team</p> <br>
			<img src= "'.get_site_url().'/wp-content/plugins/json-api/controllers/images/locationIcon.png" style="width:14px;"> Landmark Plaza, Argwings Kodhek Road, Nairobi<br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/phoneIcon.png" style="width:14px;"> +254 706 503 230 <br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/emailIcon.png" style="width:14px;"> support@mombo.africa <br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/urlIcon.png" style="width:14px;"> www.mombo.co.ke';
			$to =  $email;
			$subject =  "Login Pin Number";
			$headers[] = 'Content-Type: text/html; charset=UTF-8';
	        $headers[] = 'From: Mombo <noreply@getyoursolutions.com>';
			if(wp_mail($to,$subject,$message,$headers))
			{
				update_user_meta($user_id,'secret_pin_number',$pin_number);
				$msg .= 'Login pin number sent';
			}else{
				$msg .= 'Login pin number not sent';
			}
			//delete_user_meta($user_id,'phone_verification_code',$phone_verification_code);
			//delete_user_meta($user_id,'email_verification_code',$email_verification_code);
			$message = $this->_requestStatus("2004");
			return array(
				"RespCode" => "2004", // Verification code correct!
				"success" => "false",
				"Message" => $msg,
				"user_data" => $user_data,
			);
		}				
	}
	
	public function pin_verification()
	{
		global $json_api,$wpdb;
		$json     = file_get_contents("php://input");
		$data     = json_decode($json);
		$user_id  = $data->user_id;
		$login_pin_no = $data->login_pin_no;
		$secret_pin = get_user_meta($user_id,'secret_pin_number',true);
		if($secret_pin != $login_pin_no)
		{
			$message = $this->_requestStatus("2023");
			return array(
				"RespCode" => "2023", // Login pin not verified!
				"success" => "false",
				"Message" => $message,
			);
		}else{
			$message = $this->_requestStatus("2024");
			return array(
				"RespCode" => "2024", // Login pin verified!
				"success" => "true",
				"Message" => $message,
			);
		}
	}
	
	public function reset_pin_verification()
	{
		global $json_api,$wpdb;
		$json     = file_get_contents("php://input");
		$data     = json_decode($json);
		$user_id  = $data->user_id;
		$email = get_user_meta($user_id,'user_email',true);
		$ar = explode('@',$email);
		//$secret_message  = $data->secret_message;
		$new_pin  = $data->new_pin_no;
		$confirm_pin  = $data->confirm_pin_no;		
		//$secret_word = get_user_meta($user_id,'secret_message',true);
		/* if($secret_message != $secret_word)
		{
				$message = $this->_requestStatus("2025");
				return array(
					"RespCode" => "2025", // Secret word not correct!
					"success" => "false",
					"Message" => $message,
				);
		}else  */
		if($new_pin != $confirm_pin){
				$message = $this->_requestStatus("2026");
				return array(
					"RespCode" => "2026", //Pin does not match!
					"success" => "false",
					"Message" => $message,
				);
	   }else{
		    $message = "<p>Dear ".$ar[0]." <br><br>";
			$message = "Your Login Reset Pin number is ".$new_pin."";
			$message .= '<p>Thank You <br> 
			The Mombo Team</p> <br>
			<img src= "'.get_site_url().'/wp-content/plugins/json-api/controllers/images/locationIcon.png" style="width:14px;"> Landmark Plaza, Argwings Kodhek Road, Nairobi<br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/phoneIcon.png" style="width:14px;"> +254 706 503 230 <br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/emailIcon.png" style="width:14px;"> support@mombo.africa <br>
			<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/urlIcon.png" style="width:14px;"> www.mombo.co.ke';
			$to =  $email;
			$subject =  "Login Reset Pin Number";
			$headers[] = 'Content-Type: text/html; charset=UTF-8';
			$headers[] = 'From: Mombo <noreply@getyoursolutions.com>';
			if(wp_mail($to,$subject,$message,$headers))
			{
				update_user_meta($user_id,'secret_pin_number',$new_pin);
			}
				$message = $this->_requestStatus("2027");
				return array(
					"RespCode" => "2027", //Login pin reset successfully!
					"success" => "true",
					"Message" => $message,
				);				
			}		
	}
	
	public function forgot_secret_word_send_otp()
	{
	    global $json_api,$wpdb;
		$json     = file_get_contents("php://input");
		$data     = json_decode($json);	
		$user_id =   $data->user_id;
		$email = get_user_meta($user_id,'user_email',true);
		$ar = explode('@',$email);
		$generate_word = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
		$len = strlen($generate_word); 
		$n=5;
		   for ($i = 0; $i < $n; $i++) 
			{ 
				$index = rand(0, $len - 1); 
				$generated_string = $generated_string . $generate_word[$index]; 
				
			}
		$message = "<p>Dear ".$ar[0]." <br><br>";
		$message = "Your new Secret word is ".$generated_string."";
		$message .= '<p>Thank You <br> 
		The Mombo Team</p> <br>
		<img src= "'.get_site_url().'/wp-content/plugins/json-api/controllers/images/locationIcon.png" style="width:14px;"> Landmark Plaza, Argwings Kodhek Road, Nairobi<br>
		<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/phoneIcon.png" style="width:14px;"> +254 706 503 230 <br>
		<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/emailIcon.png" style="width:14px;"> support@mombo.africa <br>
		<img src="'.get_site_url().'/wp-content/plugins/json-api/controllers/images/urlIcon.png" style="width:14px;"> www.mombo.co.ke';
		$to =  $email;
		$subject =  "New Secret Word Generate";
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		$headers[] = 'From: Mombo <noreply@getyoursolutions.com>';
		if(wp_mail($to,$subject,$message,$headers))
		{
			update_user_meta($user_id,'secret_message',$generated_string);
			$message = $this->_requestStatus("2042");
			return array(
				"RespCode" => "2042", //Secret word reset successfully!
				"success" => "true",
				"Message" => $message
			);	
		}					
	} 
	
	public function forgot_secret_match_word()
	{
		global $json_api;
		$json     = file_get_contents("php://input");
		$data     = json_decode($json);
		$user_id =   $data->user_id;
		$secret_word = $data->secret_message;		
		$get_secret_word = get_user_meta($user_id,'secret_message',true);
		if($secret_word != $get_secret_word)
		{
			$message = $this->_requestStatus("2025");
			return array(
				"RespCode" => "2025", //Secret word OTP code not correct!
				"success" => "false",
				"Message" => $message
			);	
		}else{		
			$message = $this->_requestStatus("2043");
			return array(
				"RespCode" => "2043", //Secret word correct!
				"success" => "false",
				"Message" => $message
			);	
	    }
	}
	
	public function login()
	{ 
		global $json_api,$wpdb;
		$json     = file_get_contents("php://input");
		$data     = json_decode($json);
		$username = $data->user_id;
		$password = $data->password;
		//$devicetype = $data->devicetype;
		$devicetoken = $data->deviceToken;
		if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
			$userData               = get_user_by('email', $username);
			$creds['user_login']    = $userData->user_login;
			$creds['user_password'] = $password;
			$creds['remember']      = true; 
			$user                   = wp_signon($creds, false);
		} else {
			$userData               = get_user_by('login', $username);
			$creds['user_login']    = $userData->user_login;
			$creds['user_password'] = $password;
			$creds['remember']      = true; 
			$user                   = wp_signon($creds, false);
		}
		if (is_wp_error($user)) { 
			$error_string = $user->get_error_message();
			if (strpos($error_string, 'password') === false) {
				$message1 = 'Email not correct';
			}else{
				$message1 = 'Password not correct';
			}
			$message = $this->_requestStatus("1005");
			return array(
				"RespCode" => "1005",  //1005 => 'Please Try Again',
				"Message" => $message1,
			);
		} else {
			$user_id = $user->ID;
			$user_email = $user->user_email;
			$user_login = $user->user_login;
			$address = get_user_meta($user_id, 'address', true);
			$area_code = get_user_meta($user_id, 'area_code', true);
			$country = get_user_meta($user_id, 'country', true);
			$mobile_number = get_user_meta($user_id, 'mobile_number', true);			
			
			$userData = get_userdata( $user_id );
			$dattime = $userData->data->user_registered;
			$date = date('Y-m-d',strtotime($dattime));
			$user_registered = date('j M Y', strtotime($date));
			update_user_meta($user_id,'devicetoken',$devicetoken);
			$user_data = array("id" => $user_id,
				"country" => $country,
				"area_code" => $area_code,
				"address" => $address,
				"mobile_number" => $mobile_number
			);
			$message = $this->_requestStatus("1001");
			return array(
				"RespCode" => "1001", //1001 => 'Login Successfully',
				"success" => "true", 
				"Message" => $message,
				"user_data" => $user_data
			);
		}
	}
	
	public function forgotpassword()
	{
		global $json_api, $wpdb;
		$json = file_get_contents("php://input");
		$data = json_decode($json);
		$user_email = $data->email;

		if($user_email == ''){
			$message = $this->_requestStatus("7002");
			return array(
				"RespCode" => "7002",
				"Message" => $message,
			);
		}
		if (email_exists($user_email)) {
			$user = get_user_by('email', $user_email);
			$username = get_user_meta( $user->ID, 'first_name', true);
			$generatedpassword = wp_generate_password();
			$updatePasswordQuery = $wpdb->update($wpdb->users, array(
					'user_pass' => md5($generatedpassword)
				), array(
					'user_email' => $user_email
				));
			
			$message .= __('Hi, ', 'simplr-reg') . "\r\n\r\n";
			$message .= __('Password has been reset for the following account:', 'simplr-reg') . "\r\n\r\n";
			//$message .= network_site_url() . "\r\n\r\n";
			$message .= sprintf(__('Username: %s', 'simplr-reg'), $user->user_login) . "\r\n\r\n";
			$message .= sprintf(__('Email: %s', 'simplr-reg'), $user_email) . "\r\n\r\n";
			$message .= __('Following is new generated password:', 'simplr-reg') . "\r\n\r\n";
			$message .= $generatedpassword . "\r\n\r\n";
			$message .= __('Regards,', 'simplr-reg') . "\r\n\r\n";
			
			if (is_multisite())
				$blogname = $GLOBALS['current_site']->site_name; 
			else
			// The blogname option is escaped with esc_html on the way into the database in sanitize_option
				
			// we want to reverse this for the plain text arena of emails.
			$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
			
			$title = sprintf(__('[%s] Password Reset', 'simplr-reg'), $blogname);
			
			$title   = apply_filters('retrieve_password_title', $title);
			$message = apply_filters('retrieve_password_message', $message, $key);
			
			if (isset($simplr_options->default_email)) {
				$from = $simplr_options->default_email;
			} else {
				$from = get_option('admin_email');
			}
			$headers = "From: " . $blogname . " <" . $from . "> \r\n";
			
			if($updatePasswordQuery){
				$returnvalue = "Password Updated".$generatedpassword;
				if (wp_mail($user_email, $title, $message, $headers)) {
					$message = $this->_requestStatus("1011");
					return array(
						"RespCode" => "1011", //Mail Sent Successfully
						"Message" => $message
					);
				} else {
					$message = $this->_requestStatus("1005");
					return array( 
						"RespCode" => "1005", //Please try again
						"Message" => $message
					);
				} 
			} else {
				$message = $this->_requestStatus("1005");
				return array(
					"RespCode" => "1005", //Please try again
					"Message" => $message
				);
			}
				
		} else {
			$message = $this->_requestStatus("1006");
			return array(
				"RespCode" => "1006", //*Email doesn't exists
				"Message" => $message
			);
		}
	}
  
  
	public function Get_countryList()
	{
		global $json_api, $wpdb;
		$json = file_get_contents("php://input");
		$data = json_decode($json);
		global $woocommerce;
		$countries_obj   = new WC_Countries();
		$preferredcountrydata = array();
		$i=0;
		$countryid= 1;  
		foreach($countries_obj->countries as $key =>  $value){
			$dd = html_entity_decode($value);			  
			$preferredcountrydata[$i]['name'] = $dd;
			$preferredcountrydata[$i]['country_code'] = $key;
			$preferredcountrydata[$i]['country_id'] = $countryid;
			$img = strtolower($key);
			$preferredcountrydata[$i]['image'] = plugins_url()."/json-api/controllers/flags-normal/".$img.'.png';		
			$today = date('Y-m-d');
			$i++;
			$countryid++;
		}
		$message = $this->_requestStatus("7012");
		return array(
			"RespCode" => "7012", 
			"success" => "true", 
			"Message" => $message,
			"Country_list"=>$preferredcountrydata
		 );
	}

	public function Get_country_callingcode()
	{
		global $json_api, $wpdb;
		$json = file_get_contents("php://input");
		$data = json_decode($json);
		$country_code = $data->country_code;
		$apiurl = 'https://restcountries.eu/rest/v1/alpha?codes='.$country_code;
		$idd=json_decode(file_get_contents($apiurl)); 
		$country_calling_code = $idd[0]->callingCodes[0];
		$message = $this->_requestStatus("7012");
		return array(
			"RespCode" => "7013",  // Country calling code fetched successfully
			"success" => "true", 
			"Message" => $message,
			"country_calling_code"=>$country_calling_code
		 );		
	}
 
	public function terms_conditions()
	{
		global $json_api, $wpdb;
		$json = file_get_contents("php://input");
		$data = json_decode($json);	
		$loop = get_post(5);
		$terms_title =  $loop->post_title;
		$terms_content =  $loop->post_content;
		$message = $this->_requestStatus("2041");
		return array(
			"RespCode" => "2041", //2041 => 'Terms and conditions fetched successfully'
			"success" => "false",
			"Message" => $message,
			"terms_title" => $terms_title,
			"terms_content" => $terms_content
		); 
	}
}

?>
