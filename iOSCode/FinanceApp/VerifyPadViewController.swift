//
//  VerifyPadViewController.swift
//  FinanceApp
//
//  Created by ThaparMac on 15/02/19.
//  Copyright © 2019 ThaparMac. All rights reserved.
//

import UIKit

class VerifyPadViewController: UIViewController {

    @IBOutlet var lblfirst:UILabel!
    @IBOutlet var lblsec:UILabel!
    @IBOutlet var lblthird:UILabel!
    @IBOutlet var lblfourth:UILabel!
    var arr:Array<Int> = []

    @IBOutlet var btnforgot:UIButton!
      @IBOutlet var lbl_first:UILabel!
      @IBOutlet var lbl_sec:UILabel!
       @IBOutlet var viewline:UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
      let main_string = String(format:"Need Assistance?Reach us through \n +254 710 94 0006 \n +254 706 50 3230")
      let string_to_color = " +254 710 94 0006"
      let string_to_color1 = "+254 706 50 3230"
      let range = (main_string as NSString).range(of: string_to_color)
      let range1 = (main_string as NSString).range(of: string_to_color1)
      let attribute = NSMutableAttributedString.init(string:main_string)
      attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white , range: range)
      attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white , range: range1)
      lbl_first.attributedText = attribute
      self.setupinital()

      if USERDEFAULTS_GET_BOOL_KEY(key: "first") == true {
            btnforgot.isHidden = true
            lbl_first.isHidden = false
            lbl_sec.isHidden = false
            viewline.isHidden = false

      }
      else
      {
            btnforgot.isHidden = false
            lbl_first.isHidden = true
            lbl_sec.isHidden = true
            viewline.isHidden = true

      }
        // Do any additional setup after loading the view.
    }
      func setupinital()
      {
            let main_string = String(format:"or \n support@mombo.africa")
            let string_to_color = "support@mombo.africa"

            let range = (main_string as NSString).range(of: string_to_color)

            let attribute = NSMutableAttributedString.init(string:main_string)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white , range: range)

            lbl_sec.attributedText = attribute


      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - action
    @IBAction func oneclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(1)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
        
        
    }
    @IBAction func twoclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(2)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
        
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @IBAction func thirdclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(3)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
        
    }
    @IBAction func fourthclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(4)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
        
    }
    @IBAction func fiveclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(5)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
        
    }
    @IBAction func sixclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(6)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
        
    }
    @IBAction func sevenclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(7)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
       
    }
    @IBAction func eightclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(8)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
        
    }
    @IBAction func nineclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(9)
            if arr.count == 4
            {
                self.callverifypin()
            }
            
        }
        
    }
    @IBAction func tenclick(sender:UIButton)
    {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            sender.alpha = 0.5
        }, completion: { finished in
            print("Basket doors opened!")
            sender.alpha = 1.0
        })
        if arr.count < 4
        {
            
            if arr.count == 0
            {
                lblfirst.text = "*"
            }
            else if arr.count == 1
            {
                lblsec.text = "*"
            }
            else if arr.count == 2
            {
                lblthird.text = "*"
            }
            else if arr.count == 3
            {
                lblfourth.text = "*"
            }
            arr.append(0)
            if arr.count == 4
            {
                self.callverifypin()
            }
        }
        
    }
    @IBAction func leftbackclick(sender:UIButton)
    {
        if USERDEFAULTS_GET_BOOL_KEY(key: "UserLoggedIn") == true {
        self.navigationController?.popViewController(animated: true)
      }
        
    }
    @IBAction func rightbackclick(sender:UIButton)
    {
        
    }
    
    @IBAction func resetclick(sender:UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "sceret")
        self.navigationController?.pushViewController(obj!, animated: true)
    }
    
    func callverifypin() -> Void {
        //let token = USERDEFAULTS.value(forKey: "DeviceToken") as! String
        let one = arr[0]
        let two = arr[1]
        let three = arr[2]
        let fourth = arr[3]
        let userid = UserDefaults.standard.value(forKey: "User_ID") as! Int
        
        let st = String(format:"%d%d%d%d",one,two,three,fourth)
        let parameters : [String:Any] = ["user_id":String(userid),"login_pin_no":st]
        print(parameters)
        POST_DATA_TO_SERVER(parameters: parameters as NSDictionary, api: pin_verification, isShowLoader: true, forView: view, success: { (success, response) in
            let responseObj = response.value as! NSDictionary
            print("User Registration: \(responseObj)")
            switch (responseObj["RespCode"] as! NSString).intValue{
            
            case 2023:
                 SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Login pin not verified!")

                  self.lblfirst.text = ""
                  self.lblsec.text = ""
                  self.lblthird.text = ""
                  self.lblfourth.text = ""
                 self.arr.removeAll()

                break
            case 2024:
                   USERDEFAULTS_SET_BOOL_KEY(object: false, key: "first")
                   USERDEFAULTS_SET_BOOL_KEY(object: true, key: "UserLoggedIn")
                // SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Login pin verified!")
                 let obj = self.storyboard?.instantiateViewController(withIdentifier: "home")
                 self.navigationController?.pushViewController(obj!, animated: true)
                break
            default:
                  self.lblfirst.text = ""
                  self.lblsec.text = ""
                  self.lblthird.text = ""
                  self.lblfourth.text = ""
                  self.arr.removeAll()
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "please try again later!")
            }
        }) { (failure, error) in
            print("Register API Error: \(error)")
            SHOW_AUTOHIDE_MESSAGE(view: self.view, message: SERVER_ERROR)
        }
    }

}
