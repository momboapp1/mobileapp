//
//  EmailVerificationViewController.swift
//  FinanceApp
//
//  Created by ThaparMac on 14/02/19.
//  Copyright © 2019 ThaparMac. All rights reserved.
//

import UIKit

class EmailVerificationViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet var viewemailcode:UIView!
    @IBOutlet var viewphonecode:UIView!
    @IBOutlet var textemailcode:UITextField!
    @IBOutlet var textphonecode:UITextField!
      @IBOutlet var img:UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupinital()
      img.isHidden = true
        // Do any additional setup after loading the view.
    }
    func setupinital()
    {
        viewemailcode.layer.cornerRadius =  viewemailcode.frame.width * 0.05
        viewemailcode.layer.masksToBounds = true
        viewphonecode.layer.cornerRadius =  viewphonecode.frame.width * 0.05
        viewphonecode.layer.masksToBounds = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: textfiels delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
       
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    
    return true
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @IBAction func btnverifyClick(sender:UIButton)
    {
       
        
        if (textemailcode?.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)! && (textphonecode?.text?.isEmpty)!
            {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Required fields.", btnTitle: "OK", viewController: self, completionHandler: { (success) in

            })

        }
        else if (textemailcode?.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)! || (textphonecode?.text?.isEmpty)!
        {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Required fields.", btnTitle: "OK", viewController: self, completionHandler: { (success) in

            })

        }else
        {
            self.view.endEditing(true)
            self.callgetcountries()
        }
//        let obj = self.storyboard?.instantiateViewController(withIdentifier: "verifypad")
//        self.navigationController?.pushViewController(obj!, animated: true)
    }
    //sttextemail sttextphone
    //MARK: WEb api
    //phone_code,email_code
    func callgetcountries() -> Void {

//phone_code,email_code,gender,country,national_id,dob,mobile_number,status,secret_message,email,deviceId
        let one :String = textphonecode.text!
        let two :String = textemailcode.text!
        //let userid = UserDefaults.standard.value(forKey: "User_ID") as! Int
      let parameters : [String:Any] = ["phone_code":one,"email_code":two,"gender":stmale,"country":stlblcountry,"national_id":sttextid,"dob":sttextdateofbirth,"mobile_number":sttextphone,"status":"","secret_message":sttextsecertword,"email":sttextemail,"deviceId":deviceId]
        print(parameters)
        POST_DATA_TO_SERVER(parameters: parameters as NSDictionary, api: code_verification, isShowLoader: true, forView: view, success: { (success, response) in
            let responseObj = response.value as! NSDictionary
            print("User Registration: \(responseObj)")
            switch (responseObj["RespCode"] as! NSString).intValue{
                
            case 2004:
            self.img.isHidden = false
             let userdata = responseObj["user_data"] as! NSDictionary
             USERDEFAULTS_SET_INT_KEY(object:userdata.value(forKey: "id") as! Int , key: "User_ID")
             
            USERDEFAULTS_SET_BOOL_KEY(object: true, key: "firsttimedone")
            // SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "please check your email for login pin number")

             DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {

                  let obj = self.storyboard?.instantiateViewController(withIdentifier: "verifypad")
                  self.navigationController?.pushViewController(obj!, animated: true)
             })

                break
            case 2001:
                //
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Verification code not correct!")
                break
            case 2002:
                //
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Email Verification code not correct!")
                break
            case 2003:
                //
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Phone Verification code not correct!")
                break
            default:
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "please try again later!")
            }
        }) { (failure, error) in
            print("Register API Error: \(error)")
            SHOW_AUTOHIDE_MESSAGE(view: self.view, message: SERVER_ERROR)
        }
    }
    
    @IBAction func btnbackClick(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func backClick(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

}
