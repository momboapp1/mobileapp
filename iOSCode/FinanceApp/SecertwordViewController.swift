//
//  SecertwordViewController.swift
//  FinanceApp
//
//  Created by Devgan-Mac on 21/02/19.
//  Copyright © 2019 ThaparMac. All rights reserved.
//

import UIKit

class SecertwordViewController: UIViewController {

      @IBOutlet var viewsecertwordcode:UIView!


      @IBOutlet var textsecertword:UITextField!


      @IBOutlet var lbl_first:UILabel!

      @IBOutlet var lbl_sec:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
      self.setupinital()

      let main_string = String(format:"Need Assistance?Reach us through \n +254 710 94 0006 \n +254 706 50 3230")
      let string_to_color = " +254 710 94 0006"
      let string_to_color1 = "+254 706 50 3230"
      let range = (main_string as NSString).range(of: string_to_color)
      let range1 = (main_string as NSString).range(of: string_to_color1)
      let attribute = NSMutableAttributedString.init(string:main_string)
      attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range)
      attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range1)
      lbl_first.attributedText = attribute
        // Do any additional setup after loading the view.
    }
      func setupinital()
      {
            let main_string = String(format:"or \n support@mombo.africa")
            let string_to_color = "support@mombo.africa"

            let range = (main_string as NSString).range(of: string_to_color)

            let attribute = NSMutableAttributedString.init(string:main_string)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range)

            lbl_sec.attributedText = attribute

            viewsecertwordcode.layer.cornerRadius =  viewsecertwordcode.frame.width * 0.05
            viewsecertwordcode.layer.masksToBounds = true



      }
      @IBAction func backclick(sender:UIButton)
      {
            self.navigationController?.popViewController(animated: true)
      }
      @IBAction func clickverify(sender:UIButton)
      {
            if (textsecertword?.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)!
            {
                  SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Required fields.", btnTitle: "OK", viewController: self, completionHandler: { (success) in

                  })

            }
            else if (textsecertword?.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)!
            {
                  SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Required fields.", btnTitle: "OK", viewController: self, completionHandler: { (success) in

                  })

            }


            else
            {
                      self.view.endEditing(true)
                  self.callresetscert()
            }
      }

      func callresetscert() -> Void {
            //let token = USERDEFAULTS.value(forKey: "DeviceToken") as! String

            let userid = UserDefaults.standard.value(forKey: "User_ID") as! Int

            let parameters : [String:Any] = ["user_id":String(userid),"secret_message":textsecertword.text]
            print(parameters)
            POST_DATA_TO_SERVER(parameters: parameters as NSDictionary, api: forgot_secret_match_word, isShowLoader: true, forView: view, success: { (success, response) in
                  let responseObj = response.value as! NSDictionary
                  print("User Registration: \(responseObj)")
                  switch (responseObj["RespCode"] as! NSString).intValue{

                  case 2025:
                        SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Secret word not correct!")
                        break

                  case 2043:
                        //SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Pin Succesfuly Reset!")
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "reset")
                        self.navigationController?.pushViewController(obj!, animated: true)
                        break
                  default:
                        SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "please try again later!")
                  }
            }) { (failure, error) in
                  print("Register API Error: \(error)")
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: SERVER_ERROR)
            }
      }
}
