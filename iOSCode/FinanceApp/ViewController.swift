//
//  ViewController.swift
//  FinanceApp
//
//  Created by ThaparMac on 14/02/19.
//  Copyright © 2019 ThaparMac. All rights reserved.
//

import UIKit
import Haneke

var stmale:String =  String()
var stlblcountry:String =  String()
var sttextid:String =  String()
var sttextdateofbirth:String =   String()
var stlblcode:String =  String()
var sttextemail:String =  String()

var sttextsecertword:String =  String()

var sttextphone:String =  String()
var deviceId =   String()


class ViewController: UIViewController ,UITextFieldDelegate,UIGestureRecognizerDelegate,UITextViewDelegate{

      @IBOutlet var viewbgterms:UIView!
 @IBOutlet var viewbginnerterms:UIView!
      @IBOutlet var imgagree:UIImageView!
      @IBOutlet var imgdsagree:UIImageView!
@IBOutlet var imgtermsinscroll:UIImageView!


      @IBOutlet var textview:UITextView!
    var male:String = String()
    var arayCountries = NSMutableArray()
    @IBOutlet weak var table_country: UITableView!
    
    @IBOutlet var viewcounties:UIView!
    @IBOutlet var viewphoneno:UIView!
    @IBOutlet var lblcode:UILabel!
    @IBOutlet var viewemail:UIView!
    @IBOutlet var viewid:UIView!
    @IBOutlet var viewdatofbirth:UIView!
    @IBOutlet var viewsecertword:UIView!
    
    @IBOutlet var imgmale:UIImageView!
    @IBOutlet var imgfemale:UIImageView!
    @IBOutlet var imgcountry:UIImageView!
    
    @IBOutlet var lblcountry:UILabel!
    @IBOutlet var textphone:UITextField!
    @IBOutlet var textemail:UITextField!
    @IBOutlet var textid:UITextField!
    @IBOutlet var textdateofbirth:UITextField!
    @IBOutlet var textsecertword:UITextField!
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var btnsubmit:UIButton!
    @IBOutlet var viewtablecount:UIView!
    @IBOutlet var datePicker : UIDatePicker!
    @IBOutlet var viewdatepicker:UIView!
    @IBOutlet var btncount:UIButton!
      var checkterms = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupinital()
        viewtablecount.isHidden = true
        viewdatepicker.isHidden = true
        self.callgetcountries()
        male = "male"
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))

        tap.delegate = self // This is not required
        scroll.addGestureRecognizer(tap)

      viewbginnerterms.isHidden = true
      viewbgterms.isHidden = true
      viewbginnerterms.dropShadow1()
      self.callterms()
      //print(UIDevice.current.identifierForVendor?.uuidString)
      //1D669178-947D-44E5-85B3-A0B367CA62E0
        // Do any additional setup after loading the view, typically from a nib.
    }
      func textViewDidBeginEditing(_ textView: UITextView) {
            self.view.endEditing(true)
      }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
      viewdatepicker.isHidden = true
      viewtablecount.isHidden = true
      btncount.isSelected = false

    }
    // UIGestureRecognizerDelegate method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.table_country) == true {
            return false
        }
        return true
    }
//      var yourImage: UIImage = UIImage(named: "Birthday_logo")!
//      mYourImageViewOutlet.image = yourImage
    @IBAction func clickagree(sender:UIButton){


      viewbginnerterms.isHidden = true
      viewbgterms.isHidden = true
      var yourImage: UIImage = UIImage(named: "agreebtnchecked")!
      imgagree.image = yourImage
      var yourImage1: UIImage = UIImage(named: "Disagree")!
      imgdsagree.image = yourImage1

      var main: UIImage = UIImage(named: "t&cbtngreen")!
      imgtermsinscroll.image = main
      checkterms = true
      }
      @IBAction func clickdidagree(sender:UIButton){
            viewbginnerterms.isHidden = true
            viewbgterms.isHidden = true
            var yourImage: UIImage = UIImage(named: "agreebtn")!
            imgagree.image = yourImage
            var yourImage1: UIImage = UIImage(named: "Disagreechecked")!
            imgdsagree.image = yourImage1
            var main: UIImage = UIImage(named: "t&cbtnred")!
            imgtermsinscroll.image = main
            checkterms = false

      }
      @IBAction func clicktermsopen(sender:UIButton){
            viewbginnerterms.isHidden = false
            viewbgterms.isHidden = false
      }

    @IBAction func donedatePicker(sender:UIButton){

        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        textdateofbirth.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
        viewdatepicker.isHidden = true
        scroll.contentOffset.y = 0

    }
    
    @IBAction func cancelDatePicker(sender:UIButton){

        self.view.endEditing(true)
        viewdatepicker.isHidden = true
        scroll.contentOffset.y = 0
        
    }
    override func viewDidAppear(_ animated: Bool) {
        scroll.contentSize = CGSize(width: scroll.frame.width, height: scroll.frame.height + 200)
        self.setupinital()
    }
    func setupinital()
    {
        viewcounties.layer.cornerRadius =  viewcounties.frame.width * 0.06
        viewcounties.layer.masksToBounds = true
        
        viewphoneno.layer.cornerRadius =  viewphoneno.frame.width * 0.06
        viewphoneno.layer.masksToBounds = true
        
        lblcode.layer.cornerRadius =  lblcode.frame.width * 0.12
        lblcode.layer.masksToBounds = true
        
        viewemail.layer.cornerRadius =  viewemail.frame.width * 0.05
        viewemail.layer.masksToBounds = true
        
        viewid.layer.cornerRadius =  viewid.frame.width * 0.05
        viewid.layer.masksToBounds = true
        
        viewdatofbirth.layer.cornerRadius =  viewdatofbirth.frame.width * 0.05
        viewdatofbirth.layer.masksToBounds = true
        
        viewsecertword.layer.cornerRadius =  viewsecertword.frame.width * 0.05
        viewsecertword.layer.masksToBounds = true

        viewbginnerterms.layer.cornerRadius =  viewbginnerterms.frame.width * 0.08
        viewbginnerterms.layer.masksToBounds = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Actions
    @IBAction func clickmale(sender:UIButton)
    {
        let yourImage: UIImage = UIImage(named: "radio field")!
        imgmale.image = yourImage
        male = "male"
        let yourImage1: UIImage = UIImage(named: "radiounfiled")!
        imgfemale.image = yourImage1
    }
    
    @IBAction func clickfemale(sender:UIButton)
    {
        let yourImage: UIImage = UIImage(named: "radiounfiled")!
        imgmale.image = yourImage
        male = "female"
        let yourImage1: UIImage = UIImage(named: "radio field")!
        imgfemale.image = yourImage1
        
    }
    
    @IBAction func clickcountries(sender:UIButton)
    {
      if sender.isSelected
      {
            sender.isSelected = false
            if arayCountries.count > 0
            {
                  viewdatepicker.isHidden = true
                  self.view.endEditing(true)
                  viewtablecount.isHidden = true
            }
      }
      else
      {
             sender.isSelected = true
            if arayCountries.count > 0
            {
                  viewdatepicker.isHidden = true
                  self.view.endEditing(true)
                  viewtablecount.isHidden = false
            }
      }

    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @IBAction func clickdateofbirth(sender:UIButton)
    {
        
        viewtablecount.isHidden = true
        
        self.view.endEditing(true)
        viewdatepicker.isHidden = false
        scroll.contentOffset.y = 200
        
    }
    
    @IBAction func clicksubmit(sender:UIButton)
    {

      if checkterms == true
      {
            self.checkvalidation()
      }
      else
      {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please Agree with terms&conditions", btnTitle: "OK", viewController: self, completionHandler: { (success) in

            })
      }

//        let obj = self.storyboard?.instantiateViewController(withIdentifier: "verify")
//        self.navigationController?.pushViewController(obj!, animated: true)
//        let obj = self.storyboard?.instantiateViewController(withIdentifier: "home")
//        self.navigationController?.pushViewController(obj!, animated: true)
    }
    
    func checkvalidation()
    {
        if (textphone?.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)! && (textemail?.text?.isEmpty)! && (textid?.text?.isEmpty)!  && (textdateofbirth?.text?.isEmpty)!
          && (textsecertword?.text?.isEmpty)!{
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Required fields.", btnTitle: "OK", viewController: self, completionHandler: { (success) in
//                self.textphone?.text  = ""
//                self.textemail?.text = ""
//                self.textid?.text = ""
//                self.textdateofbirth?.text = ""
//                self.textsecertword.text = ""
//                self.textphone?.becomeFirstResponder()
            })
            
        }
        if (textphone?.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)! || (textemail?.text?.isEmpty)! || (textid?.text?.isEmpty)!  || (textdateofbirth?.text?.isEmpty)!
            || (textsecertword?.text?.isEmpty)!{
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Required fields.", btnTitle: "OK", viewController: self, completionHandler: { (success) in
//                self.textphone?.text  = ""
//                self.textemail?.text = ""
//                self.textid?.text = ""
//                self.textdateofbirth?.text = ""
//                self.textsecertword.text = ""
//                self.textphone?.becomeFirstResponder()
            })
            
        }else if (!(IS_VALID_EMAIL(emailId: (textemail?.text!)!))){
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter valid email address.", btnTitle: "OK", viewController: self, completionHandler: { (success) in
                self.textemail?.becomeFirstResponder()
            })
        }
        else if lblcode.text == ""
        {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Country code.", btnTitle: "OK", viewController: self, completionHandler: { (success) in
                self.textemail?.becomeFirstResponder()
            })
            
        }
        else if lblcountry.text == ""
        {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please select country.", btnTitle: "OK", viewController: self, completionHandler: { (success) in
                self.textemail?.becomeFirstResponder()
            })
        }
        else
        {
            self.callregister()
        }
    }
    //MARK: textfiels delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        viewtablecount.isHidden = true
        viewdatepicker.isHidden = true
        if textphone == textField
        {
            
        }
        else  if textemail == textField
        {
            scroll.contentOffset.y = 100
        }
        else  if textid == textField
        {
            scroll.contentOffset.y = 150
        }
        else  if textsecertword == textField
        {
            scroll.contentOffset.y = 350
            
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewtablecount.isHidden = true
        viewdatepicker.isHidden = true
        if textphone == textField
        {
            textemail.becomeFirstResponder()
        }
        else  if textemail == textField
        {
            textid.becomeFirstResponder()
        }
        else  if textid == textField
        {
            textsecertword.becomeFirstResponder()
        }
        else  if textsecertword == textField
        {
            
            textsecertword.resignFirstResponder()
        }
        return true
    }
    
    //MARK: WEb api
    
    func callgetcountries() -> Void {
        //let token = USERDEFAULTS.value(forKey: "DeviceToken") as! String
        let parameters : [String:Any] = ["":""]
        print(parameters)
        POST_DATA_TO_SERVER(parameters: parameters as NSDictionary, api: Get_countryList, isShowLoader: true, forView: view, success: { (success, response) in
            let responseObj = response.value as! NSDictionary
            print("User Registration: \(responseObj)")
            switch (responseObj["RespCode"] as! NSString).intValue{
                
            case 7012:
                let countrydata = responseObj["Country_list"] as! NSArray
                self.arayCountries = NSMutableArray(array: countrydata)
                
                let dic = self.arayCountries.object(at: 116) as! NSDictionary
                self.lblcountry.text = (dic.value(forKey: "name") as! String)
                let urluserimage = URL(string:((dic.value(forKey: "image") as? String)?.replacingOccurrences(of: " ", with: "%20"))!)!
                self.imgcountry.hnk_setImageFromURL(urluserimage)
                self.table_country.reloadData()
                let code = dic.value(forKey: "country_code") as! String
                stlblcode = code
                self.callgetcountriecode(code: code)
                break

            default:
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Unable to fetch country List, please try again later!")
            }
        }) { (failure, error) in
            print("Register API Error: \(error)")
            SHOW_AUTOHIDE_MESSAGE(view: self.view, message: SERVER_ERROR)
        }
    }
    
    func callgetcountriecode(code:String) -> Void {
        //let token = USERDEFAULTS.value(forKey: "DeviceToken") as! String
        let parameters : [String:Any] = ["country_code":code]
        print(parameters)
        POST_DATA_TO_SERVER(parameters: parameters as NSDictionary, api: Get_country_calling_code, isShowLoader: true, forView: view, success: { (success, response) in
            let responseObj = response.value as! NSDictionary
            print("User Registration: \(responseObj)")
            switch (responseObj["RespCode"] as! NSString).intValue{
                
            case 7013:
                if let countrydata = responseObj["country_calling_code"] as? String
                {
                    self.lblcode.text = countrydata
                }
                else
                {
                    self.lblcode.text = ""
                }
                
                break
                
            default:
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Unable to fetch country code, please try again later!")
            }
        }) { (failure, error) in
            print("Register API Error: \(error)")
            SHOW_AUTOHIDE_MESSAGE(view: self.view, message: SERVER_ERROR)
        }
    }
    
    
    func callregister() -> Void {
  
      stmale =  male
      stlblcountry =  lblcountry.text!
      sttextid =  textid.text!
      sttextdateofbirth =  textdateofbirth.text!
      //stlblcode =  lblcode.text!
      sttextemail =  textemail.text!

      sttextsecertword =  textsecertword.text!

      sttextphone =   textphone.text!
      deviceId = (UIDevice.current.identifierForVendor?.uuidString)!
     // country,country_code,national_id,dob,mobile_number,email,deviceId


      let parameters : [String:Any] = ["country":stlblcountry,"country_code":stlblcode,"national_id":sttextid,"dob":sttextdateofbirth,"email":sttextemail,"mobile_number":sttextphone,"deviceId":deviceId]

        print(parameters)
        POST_DATA_TO_SERVER(parameters: parameters as NSDictionary, api: signup, isShowLoader: true, forView: view, success: { (success, response) in
            let responseObj = response.value as! NSDictionary
            print("User Registration: \(responseObj)")
            switch (responseObj["RespCode"] as! NSString).intValue{
                
            case 1004:
//                let userdata = responseObj["user_data"] as! NSDictionary
//                USERDEFAULTS_SET_INT_KEY(object:userdata.value(forKey: "id") as! Int , key: "User_ID")
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Email verification code sent")
                  USERDEFAULTS_SET_BOOL_KEY(object: true, key: "first")
                  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "verify")
                        self.navigationController?.pushViewController(obj!, animated: true)
                  })

            break
            case 2028:
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Age is less than 18 year")
                  break
            case 1012:
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Username already exists")
                  break
            case 2006:
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "User id must not be blank")
                  break
            case 2029:
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "National ID/ Passport number exists")
                  break
            case 2030:
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Mobile number already exists")
                  break

                case 1009:
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Email already exists")
                   break
            default:
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "please try again later!")
            }
        }) { (failure, error) in
            print("Register API Error: \(error)")
            SHOW_AUTOHIDE_MESSAGE(view: self.view, message: SERVER_ERROR)
        }
    }
    //

      func callterms() -> Void {


            let parameters : [String:Any] = ["":""]

            print(parameters)
            POST_DATA_TO_SERVER(parameters: parameters as NSDictionary, api: terms_conditions, isShowLoader: true, forView: view, success: { (success, response) in
                  let responseObj = response.value as! NSDictionary
                  print("User Registration: \(responseObj)")
                  switch (responseObj["RespCode"] as! NSString).intValue{

                  case 2041:
                        var arr = responseObj.value(forKey: "terms_content") as! String// terms_title
                        let terms_title = responseObj.value(forKey: "terms_title") as! String
                        self.textview.text = String(format:"%@ \n %@",terms_title,arr.htmlToString)
                        break

                  default:
                        SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "please try again later!")
                  }
            }) { (failure, error) in
                  print("Register API Error: \(error)")
                  SHOW_AUTOHIDE_MESSAGE(view: self.view, message: SERVER_ERROR)
            }
      }

}
//MARK:- Table View Delegates and Datasources
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arayCountries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = CLEAR_COLOR
        let dic = arayCountries.object(at: indexPath.row) as! NSDictionary
        cell.textLabel?.text = dic.value(forKey: "name") as! String
        //print(indexPath.row)
          //  print(cell.textLabel?.text)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dic = self.arayCountries.object(at: indexPath.row) as! NSDictionary
        lblcountry.text = dic.value(forKey: "name") as! String
        let urluserimage = URL(string:((dic.value(forKey: "image") as? String)?.replacingOccurrences(of: " ", with: "%20"))!)!
        self.imgcountry.hnk_setImageFromURL(urluserimage)
        let code = dic.value(forKey: "country_code") as! String

        stlblcode = code
        self.callgetcountriecode(code: code)
        viewtablecount.isHidden = true
        btncount.isSelected = false
    }

}
extension UIView {

      // OUTPUT 1
      func dropShadow(scale: Bool = true) {
            layer.masksToBounds = false
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 0.5
            layer.shadowOffset = CGSize(width: -1, height: 1)
            layer.shadowRadius = 0.5

            layer.shadowPath = UIBezierPath(rect: bounds).cgPath
            layer.shouldRasterize = true
            layer.rasterizationScale = scale ? UIScreen.main.scale : 1
      }
      func dropShadow1(scale: Bool = true) {
            layer.masksToBounds = false
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 1.0
            layer.shadowOffset = CGSize(width: -1, height: 1)
            layer.shadowRadius = 1

            layer.shadowPath = UIBezierPath(rect: bounds).cgPath
            layer.shouldRasterize = true
            layer.rasterizationScale = scale ? UIScreen.main.scale : 1
      }
      func dropShadow2(scale: Bool = true) {
            layer.masksToBounds = false
            layer.shadowColor = UIColor.lightGray.cgColor
            layer.shadowOpacity = 0.3
            layer.shadowOffset = CGSize(width: -1, height: 1)
            layer.shadowRadius = 0.2

            layer.shadowPath = UIBezierPath(rect: bounds).cgPath
            layer.shouldRasterize = true
            layer.rasterizationScale = scale ? UIScreen.main.scale : 1
      }
      // OUTPUT 2
      func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
            layer.masksToBounds = false
            layer.shadowColor = color.cgColor
            layer.shadowOpacity = opacity
            layer.shadowOffset = offSet
            layer.shadowRadius = radius

            layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            layer.shouldRasterize = true
            layer.rasterizationScale = scale ? UIScreen.main.scale : 1
      }
}
extension String {
      var htmlToAttributedString: NSAttributedString? {
            guard let data = data(using: .utf8) else { return NSAttributedString() }
            do {
                  return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            } catch {
                  return NSAttributedString()
            }
      }
      var htmlToString: String {
            return htmlToAttributedString?.string ?? ""
      }
}
