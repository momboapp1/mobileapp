//
//  HomeViewController.swift
//  FinanceApp
//
//  Created by ThaparMac on 15/02/19.
//  Copyright © 2019 ThaparMac. All rights reserved.
//

import UIKit

class HomeViewController: ParentVC {

    @IBOutlet var table:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      ADD_MENU_BUTTON_TO_VIEW()
        // Do any additional setup after loading the view.
    }
//ADD_BACK_BUTTON_TO_VIEW()
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
//MARK:- Table View Delegates and Datasources
extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = CLEAR_COLOR
        let tit = cell.contentView.viewWithTag(10) as! UILabel
        let val = cell.contentView.viewWithTag(11) as! UILabel
        if indexPath.row == 0
        {
            tit.text = "Quick Loan"
            val.text = "3,000"
        }
        else if indexPath.row == 1
        {
            tit.text = "Payday"
            val.text = "25,000"
        }
        else if indexPath.row == 2
        {
            tit.text = "Asset"
            val.text = "30% Assest Value"
        }
        else if indexPath.row == 3
        {
            tit.text = "Insta Loan"
            val.text = "50,000"
        }
        else if indexPath.row == 4
        {
            tit.text = "Lifestyle Access Loan"
            val.text = "300,000"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
