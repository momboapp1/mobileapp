//
//  ResetPinViewController.swift
//  FinanceApp
//
//  Created by ThaparMac on 15/02/19.
//  Copyright © 2019 ThaparMac. All rights reserved.
//

import UIKit

class ResetPinViewController: UIViewController,UITextFieldDelegate {
    

    @IBOutlet var viewenternewpin1:UIView!
    @IBOutlet var viewenternewpin2:UIView!
    

    @IBOutlet var textenternewpin1:UITextField!
    @IBOutlet var textenternewpin2:UITextField!
    
    @IBOutlet var lbl_first:UILabel!

    @IBOutlet var lbl_sec:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupinital()
        
        let main_string = String(format:"Need Assistance?Reach us through \n +254 710 94 0006 \n +254 706 50 3230")
        let string_to_color = " +254 710 94 0006"
        let string_to_color1 = "+254 706 50 3230"
        let range = (main_string as NSString).range(of: string_to_color)
        let range1 = (main_string as NSString).range(of: string_to_color1)
        let attribute = NSMutableAttributedString.init(string:main_string)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range1)
        lbl_first.attributedText = attribute
        
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
//            return false
//        }
//        return true
//    }
    func setupinital()
    {
        let main_string = String(format:"or \n support@mombo.africa")
        let string_to_color = "support@mombo.africa"
        
        let range = (main_string as NSString).range(of: string_to_color)
        
        let attribute = NSMutableAttributedString.init(string:main_string)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range)
 
        lbl_sec.attributedText = attribute
        
       
        
        viewenternewpin1.layer.cornerRadius =  viewenternewpin1.frame.width * 0.05
        viewenternewpin1.layer.masksToBounds = true
        
        viewenternewpin2.layer.cornerRadius =  viewenternewpin2.frame.width * 0.05
        viewenternewpin2.layer.masksToBounds = true
        
    }
    @IBAction func clickverify(sender:UIButton)
    {
        if  (textenternewpin1?.text?.isEmpty)! && (textenternewpin2?.text?.isEmpty)!
        {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Required fields.", btnTitle: "OK", viewController: self, completionHandler: { (success) in
                
            })
            
        }
        else if  (textenternewpin1?.text?.isEmpty)! || (textenternewpin2?.text?.isEmpty)!
        {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter Required fields.", btnTitle: "OK", viewController: self, completionHandler: { (success) in
                
            })
            
        }
        else if textenternewpin1?.text?.count != 4
        {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter four number pin", btnTitle: "OK", viewController: self, completionHandler: { (success) in
                
            })
            
        }
        else if textenternewpin1?.text?.count != 4
        {
            SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle: "Error", message: "Please enter four number pin", btnTitle: "OK", viewController: self, completionHandler: { (success) in
                
            })
            
        }

        else
        {
            self.callreset()
        }
    }
    @IBAction func clickback(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callreset() -> Void {
        //let token = USERDEFAULTS.value(forKey: "DeviceToken") as! String
            self.view.endEditing(true)
        let userid = UserDefaults.standard.value(forKey: "User_ID") as! Int
              
      let parameters : [String:Any] = ["user_id":String(userid),"new_pin_no":textenternewpin1.text,"confirm_pin_no":textenternewpin2.text]
        print(parameters)
        POST_DATA_TO_SERVER(parameters: parameters as NSDictionary, api: reset_pin_verification, isShowLoader: true, forView: view, success: { (success, response) in
            let responseObj = response.value as! NSDictionary
            print("User Registration: \(responseObj)")
            switch (responseObj["RespCode"] as! NSString).intValue{
                

            case 2026:
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Pin not matched!")
               
                break
            case 2027:
                 
                  let obj = self.storyboard?.instantiateViewController(withIdentifier: "verifypad")
                  self.navigationController?.pushViewController(obj!, animated: true)
                break
            default:
                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "please try again later!")
            }
        }) { (failure, error) in
            print("Register API Error: \(error)")
            SHOW_AUTOHIDE_MESSAGE(view: self.view, message: SERVER_ERROR)
        }
    }
}
