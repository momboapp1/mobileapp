//  Copyright © 2017 ThaparMac. All rights reserved.

import UIKit
import Haneke
class MenuVC: UIViewController {
    //MARK:- Variables Declaration
    var array_MenuOptions = [Dictionary<String,String>]()
    var arrayArabtitles = NSMutableArray()
    //MARK:- Outlets Declaration
    @IBOutlet weak var img_User: UIImageView!
    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet weak var lbl_desgnation: UILabel!
    @IBOutlet weak var table_Menu: UITableView!
    @IBOutlet weak var constraint_MenuHeight: NSLayoutConstraint!
    
    //MARK:- Load View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //Disable Interactive pop gesture (back swipe from edge)
      self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.viewInitialSetUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        if UIDevice().SCREEN_TYPE == .iPhone5E  || UIDevice().SCREEN_TYPE == .iPhoneX || UIDevice().SCREEN_TYPE == .iPhone8{
                    print(img_User.frame)
                   img_User.frame = CGRect(x: img_User.frame.origin.x, y: img_User.frame.origin.y, width: img_User.frame.width, height: img_User.frame.width)
                     print(img_User.frame)
            }
        
        img_User.layer.borderWidth = 2.0
        
        img_User.layer.borderColor = UIColor.white.cgColor
        
        img_User.layer.cornerRadius = img_User.bounds.width/2
        img_User.clipsToBounds = true
        img_User.contentMode = .scaleAspectFill
    }
      //      Message = "Myhr login status";
      //      RespCode = 2014;
      //      Status = "<null>";
      //      designation = "";
      //      employeeCode = kevins;
      //      "local_userid" = 3;
      //      status = ok;
      //      "user_image" = "";
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       // USERDEFAULTS_SET_Designation_KEY(object: responseObj["designation"] as? String ?? "", key: "designation")
            lbl_UserName.text = USERDEFAULTS_GET_STRING_KEY(key: "employeeCode")
//        lbl_UserName.text = USERDEFAULTS_GET_STRING_KEY(key: "User_Name")
        lbl_desgnation.text = UserDefaults.standard.value(forKey: "designation") as? String
        if USERDEFAULTS_GET_STRING_KEY(key: "Profile_Image").isEmpty == false {
            let url = URL(string:USERDEFAULTS_GET_STRING_KEY(key: "Profile_Image").replacingOccurrences(of: " ", with: "%20"))!
            img_User.hnk_setImageFromURL(url)
        }
        else
        {

            let url = URL(string:"http://223.196.72.250/sahara/wp-content/plugins/json-api/controllers/dummy-image.png".replacingOccurrences(of: " ", with: "%20"))!
            img_User.hnk_setImageFromURL(url)
            //
            
//            let image: UIImage = UIImage(named: "Userimg")!
//            img_User.image =  image

        }
        

        
//        img_User.layer.borderWidth = 2.0
//
//        img_User.layer.borderColor = UIColor.white.cgColor
//
//        img_User.layer.cornerRadius = img_User.bounds.width/2
//        img_User.clipsToBounds = true
//        img_User.contentMode = .scaleAspectFill
        //img_User.image = UIImage(named:"logo")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    //MARK:- Button Methods
    @IBAction func profileopenTapped(_ sender: Any) {
        
        self.openViewControllerBasedOnIdentifier(identifier: "EditProfile", isAnimated: true)
    }
    @IBAction func closeMenuTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
      @IBAction func logoutclick(_ sender: Any) {
            
            USERDEFAULTS_SET_BOOL_KEY(object: false, key: "UserLoggedIn")
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "verifypad")
            self.navigationController?.pushViewController(obj!, animated: true)
      }
     //MARK:- Custom Methods
    func viewInitialSetUp() {
        
        self.view.backgroundColor = BLACK_COLOR.withAlphaComponent(0.45)
        table_Menu.tableFooterView = UIView()
        table_Menu.showsVerticalScrollIndicator = false
        table_Menu.bounces = false
        self.updateMenuOptionsArray()
      
    }
    
    func updateMenuOptionsArray() {
        //arrayArabtitles = []

        array_MenuOptions.append(["Title":"CEO Message","Icon":"ceoIcon"])//رسالة الرئيس التنفيذي
        array_MenuOptions.append(["Title":"Employee Manual","Icon":"employeeManualIcon"])//كتيبات الموظف
        array_MenuOptions.append(["Title":"Organization Structure","Icon":"organizationIcon"])//الهيكل التنظيمي
        array_MenuOptions.append(["Title":"My Profile","Icon":"leftmenuprofile"])//تعديل الملف الشخصي
        array_MenuOptions.append(["Title":"Employee News","Icon":"employeeNewsIcon"])//أخبار الموظف
    array_MenuOptions.append(["Title":"Announcements","Icon":"cmpnyAnnouncementIcon"])//إعلانات الشركة
        array_MenuOptions.append(["Title":"Talent Acquisition","Icon":"imgtalent"])//موهبة اكتساب
        array_MenuOptions.append(["Title":"MyHR","Icon":"hr"])
        array_MenuOptions.append(["Title":"Events","Icon":"event"])//أخبار الحدث
        //نظام الموارد البشرية
        
        array_MenuOptions.append(["Title":"Logout","Icon":"logOut"])//الخروج
        
//        array_MenuOptions.append(["Title":"Company Announcements","Icon":"cmpnyAnnouncementIcon"])
//        array_MenuOptions.append(["Title":"Talent Acquisition","Icon":"talentIcon"])
//        array_MenuOptions.append(["Title":"CEO Message","Icon":"ceoIcon"])
//        array_MenuOptions.append(["Title":"Employee Manuals","Icon":"employeeManualIcon"])
//        array_MenuOptions.append(["Title":"Organization Structure","Icon":"organizationIcon"])
//        array_MenuOptions.append(["Title":"Edit Profile","Icon":"leftmenuprofile"])
//        array_MenuOptions.append(["Title":"Logout","Icon":"logOut"])
        //table_Menu.backgroundColor = APP_COLOR
        self.table_Menu.delegate = self
        self.table_Menu.dataSource = self
        table_Menu.reloadData()
    }
    func openViewControllerBasedOnIdentifier(identifier:String, isAnimated:Bool) -> Void {
        let button = UIButton()
        self.closeMenuTapped(button)
        let destinationViewController : UIViewController! = (self.storyboard!.instantiateViewController(withIdentifier: identifier))
        let topViewController : UIViewController = self.navigationController!.topViewController!
        if topViewController.restorationIdentifier == destinationViewController.restorationIdentifier {
            let button = UIButton()
            self.closeMenuTapped(button)
        } else {
            for viewController in self.navigationController!.viewControllers{
                if viewController.restorationIdentifier == destinationViewController.restorationIdentifier {
                    _ = self.navigationController?.popToViewController(viewController, animated: false)
                    return
                }
            }
            if topViewController.restorationIdentifier != destinationViewController.restorationIdentifier {
                _ =  self.navigationController?.pushViewController(destinationViewController, animated: isAnimated)
            }
        }
    }
    func logoutUserFromApp() {
        SHOW_ALERT_CONTROLLER_DOUBLE_BUTTON(alertTitle: ALERT, message: "Are you sure you want to logout?", btnTitle1: "Cancel", btnTitle2: "Logout", viewController: self) { (response) in
            if response.caseInsensitiveCompare("Button2") == .orderedSame {
               // self.callLogoutCurrentUser()
            }
        }
    }
}

//MARK:- Table View Delegates and Datasources
extension MenuVC : UITableViewDelegate, UITableViewDataSource {
//    CEO Message = رسالة المدير التنفيذي
    //    Employee Manual = دليل الموظف
    //    Organization Structure = الهيكل التنظيمي
    //    My Profile = الملف الشخصي
//    Employee News = أخبار الموظفين
//    Announcements = التعاميم


//    Talent Acquisition = استقطاب المواهب

    
    func addtitle(index:Int,lable:UILabel) -> Void {
        
        if index == 0
        {
            lable.text = "رسالة الرئيس التنفيذي"
        }
        if index == 1
        {
             lable.text = "دليل الموظف"
        }
        if index == 2
        {
          lable.text = "الهيكل التنظيمي"
        }
        if index == 3
        {
           lable.text = "ملفي الشخصي"
        }
        if index == 4
        {
            lable.text = "أخبار الموظفين"
        }
        if index == 5
        {
            lable.text = "الإعلانات"
        }
        if index == 6
        {
            lable.text = "استقطاب المواهب"
        }
        if index == 7
        {
            lable.text = "نظام الموارد البشرية"
        }
        if index == 8
        {
            lable.text = "الفعاليات"
        }
        if index == 9
        {
            lable.text = "الخروج"
        }
        lable.backgroundColor = CLEAR_COLOR
        lable.textColor = WHITE_COLOR
        SET_LABEL_FONT(label: lable, font: MEDIUM_FONT, size: GET_FONT_SIZE(temp: 16))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.view.bounds.width * 0.155)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_MenuOptions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell")!
     //   cell.backgroundColor = APP_COLOR
        cell.contentView.backgroundColor = CLEAR_COLOR
        let img_Menu = cell.contentView.viewWithTag(100) as! UIImageView
        img_Menu.image = UIImage(named:array_MenuOptions[indexPath.row]["Icon"]!)
        
        let lbl_Title = cell.contentView.viewWithTag(200) as! UILabel
        lbl_Title.text = array_MenuOptions[indexPath.row]["Title"]
        lbl_Title.backgroundColor = CLEAR_COLOR
        lbl_Title.textColor = WHITE_COLOR
        SET_LABEL_FONT(label: lbl_Title, font: REGULAR_FONT, size: GET_FONT_SIZE(temp: 16))
        ///
        let lbl_TitleArb = cell.contentView.viewWithTag(201) as! UILabel
        
        self.addtitle(index: indexPath.row, lable: lbl_TitleArb)
//        lbl_TitleArb.text = array_MenuOptions[indexPath.row]["Title"]
//        lbl_TitleArb.backgroundColor = CLEAR_COLOR
//        lbl_TitleArb.textColor = WHITE_COLOR
//        SET_LABEL_FONT(label: lbl_TitleArb, font: MEDIUM_FONT, size: GET_FONT_SIZE(temp: 16))
        
        ///
        let bgColorView = UIView()
      //  bgColorView.backgroundColor = APP_ORANGE_COLOR
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if indexPath.row == 0 {
                //CEO Message
                self.openViewControllerBasedOnIdentifier(identifier: "CeoMessages", isAnimated: true)
                
            }else if indexPath.row == 1 {
                //Employee Manuals
                self.openViewControllerBasedOnIdentifier(identifier: "EmployeeManuals", isAnimated: true)
               
            }else if indexPath.row == 2 {
                //Organization Structure
                self.openViewControllerBasedOnIdentifier(identifier: "OrganizationStructure", isAnimated: true)
                
            }else if indexPath.row == 3 {
               
                  self.openViewControllerBasedOnIdentifier(identifier: "EditProfile", isAnimated: true)

//                SHOW_AUTOHIDE_MESSAGE(view: self.view, message: "Under Progress")
               
            }else if indexPath.row == 4 {
                
                self.openViewControllerBasedOnIdentifier(identifier: "EmployeeNewsVC", isAnimated: true)
                
                
            }else if indexPath.row == 5 {
                //Company Announcements
                self.openViewControllerBasedOnIdentifier(identifier: "CompanyAnnouncements", isAnimated: true)
                
            }else if indexPath.row == 6 {
                //Talent Acquisition
                self.openViewControllerBasedOnIdentifier(identifier: "TalentAcquisition", isAnimated: true)
                
            }else if indexPath.row == 7 {
                  self.openViewControllerBasedOnIdentifier(identifier: "MyHR", isAnimated: true)
            }
            else if indexPath.row == 8 {
                self.openViewControllerBasedOnIdentifier(identifier: "EventNews", isAnimated: true)
               
            }
            else if indexPath.row == 9 {
                //Logout
                self.logoutUserFromApp()
            }
        }
    }
}

//MARK:- Web service Methods
extension MenuVC {

}

