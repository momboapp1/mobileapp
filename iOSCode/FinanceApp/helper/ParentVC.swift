//  Copyright © 2017 ThaparMac. All rights reserved.

import UIKit

class ParentVC: UIViewController {
      //MARK:- Variables Declaration
      let tabBar = UITabBar()

      //MARK:- Outlets Declaration

      //MARK:- Load View Methods
      override func viewDidLoad() {
            super.viewDidLoad()
            //Disable Interactive pop gesture (back swipe from edge)
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
      }

      override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(true)
      }

      //MARK:- Custom Methods
      //**************** Menu Button ************************//
      func ADD_MENU_BUTTON_TO_VIEW() {
            let btn_Menu = UIButton(type: UIButtonType.custom)
            btn_Menu.setImage(UIImage(named:"menuIcon"), for: UIControlState())
            if UIDevice().SCREEN_TYPE == .iPhoneX {
                  btn_Menu.frame = CGRect(x: 0, y: 45, width: UIScreen.main.bounds.width*0.1449, height: UIScreen.main.bounds.width*0.1207)
            }else if UIDevice().SCREEN_TYPE == .iPhone4  || UIDevice().SCREEN_TYPE == .iPhone8 || UIDevice().SCREEN_TYPE == .iPhone8Plus || UIDevice().SCREEN_TYPE == .iPhone5E{
                  btn_Menu.frame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width*0.1449, height: UIScreen.main.bounds.width*0.1207)
            }
            else
            {
                  btn_Menu.frame = CGRect(x: 0, y: 45, width: UIScreen.main.bounds.width*0.1449, height: UIScreen.main.bounds.width*0.1207)
            }

            btn_Menu.addTarget(self, action: #selector(self.menuTapped(_:)), for: UIControlEvents.touchUpInside)
            self.view.addSubview(btn_Menu)
      }
      @objc func menuTapped(_ sender : UIButton){
            sender.isEnabled = false
            let menuVC : MenuVC = self.storyboard?.instantiateViewController(withIdentifier: "Menu") as! MenuVC
            self.view.addSubview(menuVC.view)
            self.addChildViewController(menuVC)
            menuVC.view.layoutIfNeeded()
            menuVC.view.frame=CGRect(x: 0 - self.view.bounds.width, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                  menuVC.view.frame=CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height);
                  let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                  DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        sender.isEnabled = true
                  }
            }, completion:nil)
      }
      //*********************************************************//

      //**************** Back Button ************************//
      func ADD_BACK_BUTTON_TO_VIEW() {
            let btn_Back = UIButton(type: UIButtonType.custom)
            btn_Back.setImage(UIImage(named:"backIcon"), for: UIControlState())
            if UIDevice().SCREEN_TYPE == .iPhoneX {
                  btn_Back.frame = CGRect(x: (UIScreen.main.bounds.width - (UIScreen.main.bounds.width*0.1449)), y: 44, width: UIScreen.main.bounds.width*0.1449, height: UIScreen.main.bounds.width*0.1207)
            }else {
                  btn_Back.frame = CGRect(x: (UIScreen.main.bounds.width - (UIScreen.main.bounds.width*0.1449)), y: 20, width: UIScreen.main.bounds.width*0.1449, height: UIScreen.main.bounds.width*0.1207)
            }
            btn_Back.addTarget(self, action: #selector(self.backTapped(_:)), for: UIControlEvents.touchUpInside)
            self.view.addSubview(btn_Back)
      }
      @objc func backTapped(_ sender : UIButton){
            _ = self.navigationController?.popViewController(animated: true)
      }
      //*********************************************************//

      //**************** Tab Bar ************************//
      func ADD_TAB_BAR_TO_VIEW() {
            let bottomView = UIView()
            if UIDevice().SCREEN_TYPE == .iPhoneX {
                  //bottom safe area = 34 px
                  bottomView.frame = CGRect(x: 0, y: self.view.bounds.height-84, width: self.view.bounds.width, height: 84)
            }else {
                  bottomView.frame = CGRect(x: 0, y: self.view.bounds.height-50, width: self.view.bounds.width, height: 50)
            }
            self.view.addSubview(bottomView)

            let lbl_Border = UILabel(frame: CGRect(x: 0, y: 1, width: self.view.bounds.width, height: 1))
            lbl_Border.text = nil
            lbl_Border.backgroundColor = UIColor(displayP3Red: 104.0/225.0, green: 104.0/225.0, blue: 104.0/225.0, alpha: 1.0)
            bottomView.addSubview(lbl_Border)

            tabBar.frame = CGRect(x: 0, y: 1, width: self.view.bounds.width, height: 49)
            //tabBar.barTintColor = APP_BG_COLOR
            tabBar.tag = 1000
            tabBar.delegate = self
            bottomView.addSubview(tabBar)

            let tabItem_1 = UITabBarItem()
            tabItem_1.title = "Services"
            tabItem_1.image = UIImage(named:"footerhome")?.withRenderingMode(.alwaysOriginal)
            tabItem_1.selectedImage = UIImage(named:"footerhomeactive")?.withRenderingMode(.alwaysOriginal)
            //tabItem_1.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            tabItem_1.tag = 100

            let tabItem_2 = UITabBarItem()
            tabItem_2.title = "Contractors"
            tabItem_2.image = UIImage(named:"footerlocation")?.withRenderingMode(.alwaysOriginal)
            tabItem_2.selectedImage = UIImage(named:"footerlocationactive")?.withRenderingMode(.alwaysOriginal)
            //tabItem_2.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            tabItem_2.tag = 200

            let tabItem_3 = UITabBarItem()
            tabItem_3.title = "Follow Ups"
            tabItem_3.image = UIImage(named:"footeredit")?.withRenderingMode(.alwaysOriginal)
            tabItem_3.selectedImage = UIImage(named:"footereditactive")?.withRenderingMode(.alwaysOriginal)
            //tabItem_3.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            tabItem_3.tag = 300

            let tabItem_4 = UITabBarItem()
            tabItem_4.title = "Chat"
            tabItem_4.image = UIImage(named:"footerchat")?.withRenderingMode(.alwaysOriginal)
            tabItem_4.selectedImage = UIImage(named:"footerchatactive")?.withRenderingMode(.alwaysOriginal)
            //tabItem_4.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            tabItem_4.tag = 400

            let tabItem_5 = UITabBarItem()
            tabItem_5.title = "Profile"
            tabItem_5.image = UIImage(named:"footeruser")?.withRenderingMode(.alwaysOriginal)
            tabItem_5.selectedImage = UIImage(named:"footeruseractive")?.withRenderingMode(.alwaysOriginal)
            //tabItem_5.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            tabItem_5.tag = 500

            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.red], for:.normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:WHITE_COLOR], for:.selected)

            tabBar.items = [tabItem_1, tabItem_2, tabItem_3, tabItem_4, tabItem_5]
      }
      func SELECT_TAB_BAR_ITEM(itemIndex : Int) {
            if (itemIndex >= 0 ) {
                  tabBar.selectedItem = tabBar.items?[itemIndex]
            }
      }
      //*********************************************************//
      func openViewControllerBasedOnIdentifier(_ identifier:String) -> Void {
            let destinationViewController : UIViewController! = (self.storyboard!.instantiateViewController(withIdentifier: identifier))
            let topViewController : UIViewController = self.navigationController!.topViewController!
            if topViewController.restorationIdentifier! == destinationViewController.restorationIdentifier {
                 // print("You are on same view controller")
            } else {
                  for viewController in self.navigationController!.viewControllers{
                        if viewController.restorationIdentifier == destinationViewController.restorationIdentifier {
                              _ = self.navigationController?.popToViewController(viewController, animated: true)
                              return
                        }
                  }
                  if topViewController.restorationIdentifier != destinationViewController.restorationIdentifier {
                        _ =  self.navigationController?.pushViewController(destinationViewController, animated: true)
                  }
            }
      }
}

extension ParentVC : UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 100 {
            //EmployeeNewsVC
             //self.openViewControllerBasedOnIdentifier("Dashboard")
          self.openViewControllerBasedOnIdentifier("EmployeeNewsVC")
        }else if item.tag == 200 {
           self.openViewControllerBasedOnIdentifier("Map")
        }else if item.tag == 300 {
          self.openViewControllerBasedOnIdentifier("FollowUp")
        }else if item.tag == 400 {
            self.openViewControllerBasedOnIdentifier("Chat")
        }else if item.tag == 500 {
            //service_provider, subscriber => User_Type
            if USERDEFAULTS_GET_STRING_KEY(key: "User_Type").caseInsensitiveCompare("subscriber") == .orderedSame {
               self.openViewControllerBasedOnIdentifier("SubscriberProfile")
            }else {
               self.openViewControllerBasedOnIdentifier("ContractorProfile")
            }
        }
    }
}
