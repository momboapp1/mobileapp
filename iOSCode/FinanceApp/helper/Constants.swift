//  Created by ThaparMac on 06/04/17.
//  Copyright © 2017 ThaparMac. All rights reserved.

import Foundation
import UIKit
import MBProgressHUD

//MARK:- API KEYS

//MARK:- SHARED INSTANCE
let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate!
let NOTIFICATIONCENTER = NotificationCenter.default

//MARK:- ADMOB IDS
let kAppID = ""
let kInterstitialAdUnitID = ""

//MARK:- KEYBOARD REGEX


//MARK:- ALERTS TITLE AND MESSAGES
let APPNAME = "Sngbrd"
let ERROR = "Error"
let ALERT = "Alert"
let SUCCESS = "Success"
let LOADING = "Loading..."
let INTERNET_TITLE = "Weak Internet Connectivity"
let SERVER_TITLE = "Server Error"
let CONNECTION_FAILURE = "Internet connection is weak or not available. Please try again later."
let SERVER_ERROR = "Unable to establish connection with the server. Please try again later."
let WBS_ERROR = "Unable to process your request due to some server error. Please try again later."
let LOGIN_ERROR = "The email and password you entered didn't match. Please enter correct credentials."

//MARK:- CHECK DEVICE TYPE AND SIZE

func USER_INTERFACE_IDIOM_PHONE() -> Bool {
    var UserInterfaceIdiomPhone : Bool!
    if UIDevice.current.userInterfaceIdiom == .phone{
        UserInterfaceIdiomPhone = true
    }else{
       UserInterfaceIdiomPhone = false
    }
    return UserInterfaceIdiomPhone
}

extension UIDevice {
    enum ScreenType: String {
        case iPhone4
        case iPhone5E
        case iPhone8
        case iPhone8Plus
        case iPhoneX
        case iPadRetina
        case iPadPro
        case unknown
    }
    var SCREEN_TYPE: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5E
        case 1334:
            return .iPhone8
        case 2208:
            return .iPhone8Plus
        case 2436:
            return .iPhoneX
        case 1536:
            return .iPadRetina
        case 2048:
            return .iPadPro
        default:
            return .unknown
        }
    }
}
//print(UIDevice().SCREEN_TYPE)

//MARK:- REMOVE_SPECIAL_CHARACTERS_FROM_STRING
extension String {
    var REMOVE_HTML_ENCODING_FROM_STRING: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return REMOVE_HTML_ENCODING_FROM_STRING?.string ?? ""
    }
}

//MARK:- COLORS
let CLEAR_COLOR = UIColor.clear
let BLACK_COLOR = UIColor.black
let WHITE_COLOR = UIColor.white
let LIGHTGRAY_COLOR = UIColor.lightGray
let DARKGRAY_COLOR = UIColor.darkGray
let GRAY_COLOR = UIColor.gray
let App_textfieldPlaceholdercolor = UIColor(red: 94/255.0, green: 91/255.0, blue: 91/255.0, alpha: 1.0)
let APP_ViewtextfieldColor = UIColor(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1.0)
let APP_themeColor = UIColor(red: 252/255.0, green: 50/255.0, blue: 90/255.0, alpha: 1.0)
let APP_searcgbgcolor = UIColor(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 0.63)

//MARK:- APPLICATION FONTS
let MEDIUM_FONT = "Futura-Medium"
let BOLD_FONT = "Futura-Bold"
let REGULAR_FONT = ""


func MEDIUM_FONT(s: CGFloat) -> UIFont {
    return UIFont(name: MEDIUM_FONT, size: s)! as UIFont
}
func BOLD_FONT(s: CGFloat) -> UIFont {
    return UIFont(name: BOLD_FONT, size: s)! as UIFont
}
func REGULAR_FONT(s: CGFloat) -> UIFont {
    return UIFont(name: REGULAR_FONT, size: s)! as UIFont
}

func GET_TITLE_FONT_SIZE() -> CGFloat {
    let size:CGFloat
    switch UIDevice().SCREEN_TYPE {
    case .iPhone4, .iPhone5E :
        size = 15
    case .iPhone8, .iPhoneX:
        size = 16
    case .iPhone8Plus:
        size = 18
    default:
        size = 15
    }
    return size
}

func GET_FONT_SIZE(temp: CGFloat) -> CGFloat {
    let size:CGFloat
    switch UIDevice().SCREEN_TYPE {
    case .iPhone4, .iPhone5E :
        size = temp-3
    case .iPhone8, .iPhoneX:
        size = temp-1
    case .iPhone8Plus:
        size = temp
    default:
        size = temp-3
    }
    return size
}

func SET_BUTTON_FONT(button: UIButton,font: String,size: CGFloat) -> Void {
    
    button.titleLabel!.font =  UIFont(name: font, size: GET_FONT_SIZE(temp: size))
}

func SET_BAR_BUTTON_FONT(barButton: UIBarButtonItem,font: String,size: CGFloat,color: UIColor) -> Void {
    barButton.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size: size)!,NSAttributedStringKey.foregroundColor: color], for: .normal)
}

func SET_LABEL_FONT(label: UILabel,font: String,size: CGFloat) -> Void {
    label.font =  UIFont(name: font, size: GET_FONT_SIZE(temp: size))
}

func SET_TEXTFIELD_FONT(textField: UITextField,font: String,size: CGFloat) -> Void {
    textField.font =  UIFont(name: font, size: GET_FONT_SIZE(temp: size))
}

func SET_TEXTVIEW_FONT(textView: UITextView,font: String,size: CGFloat) -> Void {
    textView.font =  UIFont(name: font, size: GET_FONT_SIZE(temp: size))
}

func SET_TITLE_LABEL_FONT(label: UILabel) -> Void {
    label.font =  UIFont(name: MEDIUM_FONT, size: GET_TITLE_FONT_SIZE())
}

//MARK:- ANIMATION METHOD
func ADD_ANIMATION_TO_VIEW(view : UIView) -> Void {
    let transition : CATransition = CATransition()
    transition.duration = 0.3
    transition.type = kCATransitionFade
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    transition.fillMode = kCAFillModeBoth
    view.layer .add(transition, forKey: "FadeAnimation")
    
}

//MARK:- EMAIL VALIDATION
func IS_VALID_EMAIL(emailId:String) -> Bool {
    let emailRegEx = "[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\\.+[a-z]+"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: emailId)
}

//MARK:- ALERT CONTROLLER METHODS

func SHOW_ALERT_CONTROLLER_SINGLE_BUTTON(alertTitle:String, message:String, btnTitle:String, viewController:UIViewController, completionHandler:@escaping (Bool) -> ())
{
    let alert = ALERT_CONTROLLER(title: alertTitle, message: message)
    let cancelAction = ALERT_CONTROLLER_CANCEL_ACTION(title: btnTitle) { (success) in
        completionHandler(true)
    }
    alert.addAction(cancelAction)
    viewController.present(alert, animated: true, completion: nil)
}

func SHOW_ALERT_CONTROLLER_DOUBLE_BUTTON(alertTitle:String, message:String, btnTitle1:String, btnTitle2:String,viewController:UIViewController,  completionHandler:@escaping (String) -> ())
{
    let alert = ALERT_CONTROLLER(title: alertTitle, message: message)
    let cancelAction = ALERT_CONTROLLER_CANCEL_ACTION(title: btnTitle1) { (success) in
        completionHandler("Button1")
    }
    alert.addAction(cancelAction)
    
    let okAction = ALERT_CONTROLLER_OK_ACTION(title: btnTitle2) { (success) in
        completionHandler("Button2")
    }
    alert.addAction(okAction)
    DispatchQueue.main.async {
        viewController.present(alert, animated: true, completion: nil)
    }
}

func ALERT_CONTROLLER(title: String, message : String) -> UIAlertController {
    return  UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
}

func ALERT_CONTROLLER_CANCEL_ACTION(title:String, completionHandler:@escaping (Bool) -> ()) -> UIAlertAction {
    return  UIAlertAction(title: title, style: UIAlertActionStyle.cancel, handler:{                                          UIAlertAction in
        completionHandler(true)
    })
}

func ALERT_CONTROLLER_OK_ACTION(title:String, completionHandler:@escaping (Bool) -> ()) -> UIAlertAction {
    return  UIAlertAction(title: title, style: UIAlertActionStyle.default, handler:{                                          UIAlertAction in
        completionHandler(true)
    })
}

//MARK:- USERDEFAULTS

let USERDEFAULTS = UserDefaults.standard

func USERDEFAULTS_SET_STRING_KEY(object:String, key:String) -> Void {
    USERDEFAULTS .set(object, forKey: key)
}
func USERDEFAULTS_GET_STRING_KEY(key:String) -> String {
    return USERDEFAULTS.object(forKey: key) as? String == nil ? "" : USERDEFAULTS.object(forKey: key) as! String
}

func USERDEFAULTS_SET_BOOL_KEY(object:Bool, key:String) -> Void {
    USERDEFAULTS .set(object, forKey: key)
}
func USERDEFAULTS_GET_BOOL_KEY(key:String) -> Bool {
    return USERDEFAULTS.object(forKey: key) as? Bool == nil ? false : USERDEFAULTS.object(forKey: key) as! Bool

}

func USERDEFAULTS_SET_INT_KEY(object:Int, key:String) -> Void {
    USERDEFAULTS .set(object, forKey: key)
}
func USERDEFAULTS_GET_INT_KEY(key:String) -> Int {
    return USERDEFAULTS.object(forKey: key) as? Int == nil ? -786 : USERDEFAULTS.object(forKey: key) as! Int
}

func USERDEFAULTS_SET_FLOAT_KEY(object:Float, key:String) -> Void {
    USERDEFAULTS .set(object, forKey: key)
}
func USERDEFAULTS_GET_FLOAT_KEY(key:String) -> Float {
   return USERDEFAULTS.object(forKey: key) as? Float == nil ? -786.0 : USERDEFAULTS.object(forKey: key) as! Float
}

func USERDEFAULTS_SET_DOUBLE_KEY(object:Double, key:String) -> Void {
    USERDEFAULTS .set(object, forKey: key)
}
func USERDEFAULTS_GET_DOUBLE_KEY(key:String) -> Double {
   return USERDEFAULTS.object(forKey: key) as? Double == nil ? -786.0 : USERDEFAULTS.object(forKey: key) as! Double
}

//MARK:- MBPROGRESS HUD METHODS
func SHOW_AUTOHIDE_MESSAGE(view: UIView, message: String) {
    let progressHUD : MBProgressHUD = MBProgressHUD.showAdded(to: view, animated: true)
    progressHUD.mode = .text
    progressHUD.label.text = message
    progressHUD.label.numberOfLines = 0
    //Move to bottm center.
    //progressHUD.offset = CGPoint(x:0.0, y:MBProgressMaxOffset)
    progressHUD.offset = CGPoint(x:0.0, y:-500)
    progressHUD .hide(animated: true, afterDelay: 1.0)
}

func SHOW_LOADING_VIEW(view: UIView, message: String) {
    let progressHUD : MBProgressHUD = MBProgressHUD.showAdded(to: view, animated: true)
    progressHUD.label.text = message
}

func REMOVE_LOADING_VIEW(view: UIView) {
    MBProgressHUD.hide(for: view, animated: true)
}

//MARK:- ADDING SHADOW TO BUTTON CORNERS

func ADD_SHADOW_TO_BUTTON(button: UIButton) {
    button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.50).cgColor
    button.layer.shadowOffset = CGSize(width: 0, height: 3)
    button.layer.shadowOpacity = 1.0
    button.layer.shadowRadius = 10.0
    button.layer.masksToBounds = false
}
func ADD_SHADOW_TO_VIEW(view: UIView) {
    view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.50).cgColor
    view.layer.shadowOffset = CGSize(width: 0, height: 3)
    view.layer.shadowOpacity = 1.0
    view.layer.shadowRadius = 10.0
    view.layer.masksToBounds = false
}

let CORNER_RADIUS : CGFloat = 4.0
let BORDER_WIDTH : CGFloat = 2.0

func CREATE_ROUND_CORNER_BUTTON(button:UIButton, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    button.layer.cornerRadius = cornerRadius
    button.layer.backgroundColor = bgColor.cgColor
    button.layer.borderWidth = borderWidth
    button.layer.borderColor = borderColor.cgColor
    button.clipsToBounds = true
    button.titleLabel?.adjustsFontSizeToFitWidth = true
}

func CREATE_ROUND_CORNER_VIEW(view:UIView, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    view.layer.cornerRadius = cornerRadius
    view.layer.backgroundColor = bgColor.cgColor
    view.layer.borderWidth = borderWidth
    view.layer.borderColor = borderColor.cgColor
    view.clipsToBounds = true
}

func CREATE_ROUND_CORNER_LABEL(label:UILabel, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    label.layer.cornerRadius = cornerRadius
    label.layer.backgroundColor = bgColor.cgColor
    label.layer.borderWidth = borderWidth
    label.layer.borderColor = borderColor.cgColor
    label.clipsToBounds = true
}

func CREATE_ROUND_CORNER_IMAGE(imageView:UIImageView, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    imageView.layer.cornerRadius = cornerRadius
    imageView.layer.backgroundColor = bgColor.cgColor
    imageView.layer.borderWidth = borderWidth
    imageView.layer.borderColor = borderColor.cgColor
    imageView.clipsToBounds = true
}

func CREATE_ROUND_CORNER_TABLEVIEW(tableView:UITableView, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    tableView.layer.cornerRadius = cornerRadius
    tableView.layer.backgroundColor = bgColor.cgColor
    tableView.layer.borderWidth = borderWidth
    tableView.layer.borderColor = borderColor.cgColor
    tableView.clipsToBounds = true
}

func CREATE_ROUND_CORNER_COLLECTIONVIEW(collectionView:UICollectionView, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    collectionView.layer.cornerRadius = cornerRadius
    collectionView.layer.backgroundColor = bgColor.cgColor
    collectionView.layer.borderWidth = borderWidth
    collectionView.layer.borderColor = borderColor.cgColor
    collectionView.clipsToBounds = true
}

func CREATE_ROUND_CORNER_TEXTVIEW(textView:UITextView, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    textView.layer.cornerRadius = cornerRadius
    textView.layer.backgroundColor = bgColor.cgColor
    textView.layer.borderWidth = borderWidth
    textView.layer.borderColor = borderColor.cgColor
    textView.clipsToBounds = true
}

func CREATE_ROUND_CORNER_TEXTFIELD(textField:UITextField, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    textField.layer.cornerRadius = cornerRadius
    textField.layer.backgroundColor = bgColor.cgColor
    textField.layer.borderWidth = borderWidth
    textField.layer.borderColor = borderColor.cgColor
    textField.clipsToBounds = true
}

func CREATE_ROUND_CORNER_SEARCH_BAR(searchBar:UISearchBar, cornerRadius:CGFloat, bgColor:UIColor, borderWidth:CGFloat, borderColor:UIColor){
    searchBar.layer.cornerRadius = cornerRadius
    searchBar.layer.backgroundColor = bgColor.cgColor
    searchBar.layer.borderWidth = borderWidth
    searchBar.layer.borderColor = borderColor.cgColor
    searchBar.clipsToBounds = true
}

//MARK:- LAYOUT CONTSTRAINT
func APPLY_HEIGHT_CONSTRAINTS_CONSTANT(constraint: NSLayoutConstraint, height: CGFloat) -> Void {
    if UIDevice().SCREEN_TYPE == .iPhone4 {
        constraint.constant = height
    }
}

//MARK:- SEARCH BAR APPEARANCE
func CHANGE_SEARCH_BAR_APPEARANCE(searchBar: UISearchBar, font: String, size: CGFloat) {
    let view = searchBar.subviews[0]
    for temp in view.subviews {
        if temp .isKind(of: NSClassFromString("UISearchBarBackground")!){
            temp.removeFromSuperview()
        }
    }
    let textField : UITextField = searchBar .value(forKey: "searchField") as! UITextField
    //To change background color
    textField.backgroundColor = CLEAR_COLOR
    //To change text color
    textField.textColor = WHITE_COLOR
    //To change text font
    SET_TEXTFIELD_FONT(textField: textField, font: font, size: GET_FONT_SIZE(temp: size))
    //To change search bar search icon
    searchBar.setImage(UIImage(named: "srchIcon"), for: .search, state: .normal)
    //To change search bar clear icon
    searchBar.setImage(UIImage(named: "clearIcon"), for: .clear, state: .normal)
    //To change placeholder text color
    let textFieldInsideUISearchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel
    textFieldInsideUISearchBarLabel?.textColor = WHITE_COLOR
}

//MARK:- GUEST LOGIN

func IS_GUEST_LOGIN(viewController : UIViewController, message: String) -> Bool {
    //let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let navigationController = APPDELEGATE?.window!.rootViewController as! UINavigationController
    if USERDEFAULTS_GET_INT_KEY(key: "User_ID") <= 0 {
        SHOW_ALERT_CONTROLLER_DOUBLE_BUTTON(alertTitle: ALERT, message: message, btnTitle1: "Cancel", btnTitle2: "Login", viewController: viewController, completionHandler: { (response) in
            if(response == "Button2"){
                _ = navigationController.popToViewController(((navigationController.viewControllers[1]) as UIViewController), animated: true)
            }
        })
        return true
    }
    return false
}

//MARK: DATE FORMATTER
extension Date {
    //"y-MM-dd"
    //"HH:mm:ss" //24-Hour
    //"h:mm a" //12 Hour with AM/PM
    static func CONVERT_DATE_TO_STRING(formatter: String, date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        return dateFormatter.string(from: date as Date)
    }
}

//MARK:- TEXT FIELD EXTENSION
extension UITextField
{
    public func MAKE_COMMON_CHANGES_TO_TEXTFIELD(textColor: UIColor, paddingLeft: Int, textAlignment: NSTextAlignment,keyboard: UIKeyboardType, returnKey: UIReturnKeyType)
    {
        if #available(iOS 9.0, *) {
            let assistant = self.inputAssistantItem;
            assistant.leadingBarButtonGroups = [];
            assistant.trailingBarButtonGroups = [];
            self.autocorrectionType = .no
            self.autocapitalizationType = .none
            self.leftView = UIView(frame:(CGRect(x:0,y:0,width:paddingLeft,height:Int(self.frame.size.height))))
            self.leftViewMode = .always
            self.textAlignment = textAlignment
            self.textColor = textColor
            self.keyboardType = keyboard
            self.returnKeyType = returnKey
        }
    }
}

//MARK:- TEXT View EXTENSION
extension UITextView
{
    public func MAKE_COMMON_CHANGES_TO_TEXTVIEW(textColor: UIColor, textAlignment: NSTextAlignment, returnKey: UIReturnKeyType)
    {
        if #available(iOS 9.0, *) {
            let assistant = self.inputAssistantItem;
            assistant.leadingBarButtonGroups = [];
            assistant.trailingBarButtonGroups = [];
            self.autocorrectionType = .no
            self.autocapitalizationType = .none
            self.textAlignment = textAlignment
            self.textColor = textColor
            self.returnKeyType = returnKey
        }
    }
}
//MARK:- IMAGE SCALING
extension UIImage {
    func SCALE_IMAGE(toSize newSize: CGSize) -> UIImage? {
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext() {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(self.cgImage!, in: newRect)
            let newImage = UIImage(cgImage: context.makeImage()!)
            UIGraphicsEndImageContext()
            return newImage
        }
        return nil
    }
}

//MARK:- BLURRED IMAGE

extension UIImageView
{
    func ADD_BLUR_EFFECT_TO_IMAGE()
    {
        let blurEffect : UIBlurEffect
        if #available(iOS 10.0, *) {
            blurEffect = UIBlurEffect(style: .prominent)
        } else {
            blurEffect = UIBlurEffect(style: .light)
        }
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
}

//MARK: ADJUST WIDTH HEIGHT BASED ON TEXT LENGTH ATTRIBUTED
extension NSAttributedString {
    func CALCULATED_HEIGHT(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.height
    }
    
    func CALCULATED_WIDTH(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.width
    }
}

//MARK: ADJUST WIDTH HEIGHT BASED ON TEXT LENGTH STRING
extension String {
    func CALCULATED_HEIGHT(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func CALCULATED_WIDTH(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.width
    }
}
//print("I love my country".CALCULATED_WIDTH(withConstrainedHeight: 30, font: AVENIR_MEDIUM(s: GET_FONT_SIZE(temp: 25))))
