
import Foundation
import Alamofire


let WBS_URL = "http://182.74.186.138/mombo/api/" // local


let Get_countryList = "Get_countryList"
let Get_country_calling_code = "Get_country_callingcode"
let signup = "signup"
let code_verification = "code_verification"
let pin_verification = "pin_verification"
let reset_pin_verification = "reset_pin_verification"
let forgot_secret_match_word = "forgot_secret_match_word"
let forgot_secret_word_send_otp = "forgot_secret_word_send_otp"
let terms_conditions = "terms_conditions"
//MARK:- API Method

func POST_DATA_TO_SERVER(parameters:NSDictionary, api:String, isShowLoader: Bool, forView: UIView, success:@escaping (_ success: Bool,_ respsonse: DataResponse<Any>) -> (),  failure:@escaping (_ failure: Bool, _ error: Error) -> ()) {
    if (Reachability()?.isReachable)! {
        if isShowLoader {
            SHOW_LOADING_VIEW(view: forView, message: "Loading...")
        }
        //let headers = ["Content-Type":"x-www-form-urlencoded"]
        let headers = ["Content-Type":"Application/json"]
        let url = String(format:"%@%@",WBS_URL,api)
        print("URL:\(url)\nParameters: \(parameters as! Parameters)")
        //.post was not working in live url so changed to .put
        Alamofire.request(url, method: .put, parameters: (parameters as! Parameters), encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            /*
             print("Request : \(response.request)")
             print("Result Value : \(response.result.value)")
             print("Result : \(response.result)")
             print("RESPONSE : \(response)")
             */
            switch response.result {
            case .success:
                if isShowLoader {
                    REMOVE_LOADING_VIEW(view: forView)
                }
                success(true, response)
            case .failure(let error):
                print("API: \(url)\nError: \(error.localizedDescription)")
                if isShowLoader {
                    REMOVE_LOADING_VIEW(view: forView)
                }
                failure(false, error)
            }
        }
    }else{
        SHOW_AUTOHIDE_MESSAGE(view: forView, message: CONNECTION_FAILURE)
    }
}

func GET_DATA_FROM_SERVER(api:String, isShowLoader: Bool, forView: UIView, success:@escaping (_ success: Bool,_ response: DataResponse<Any>) -> (),  failure:@escaping (_ failure: Bool, _ error: Error) -> ()) {
    if (Reachability()?.isReachable)! {
        if isShowLoader {
            SHOW_LOADING_VIEW(view: forView, message: "Loading...")
        }
        //let headers = ["Content-Type":"x-www-form-urlencoded"]
        let headers = ["Content-Type":"Application/json"]
        let url = String(format:"%@%@",WBS_URL,api)
        print("URL:\(url)\nNo parameters for this request.")
        Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers : headers).responseJSON { (response) in
            /*
             print("Request : \(response.request)")
             print("Result Value : \(response.result.value)")
             print("Result : \(response.result)")
             print("RESPONSE : \(response)")
             */
            switch response.result {
            case .success:
                if isShowLoader {
                    REMOVE_LOADING_VIEW(view: forView)
                }
                success(true, response)
            case .failure(let error):
                print("API: \(url)\nError: \(error.localizedDescription)")
                if isShowLoader {
                    REMOVE_LOADING_VIEW(view: forView)
                }
                failure(false, error)
            }
        }
    }else{
        SHOW_AUTOHIDE_MESSAGE(view: forView, message: CONNECTION_FAILURE)
    }
}

/*
 API for login
 URL:
 http://223.196.72.250/justalent/api/login
 
 Variables Required:
 email, password, devicetype, devicetoken
 
 Response Code:
 1001 => 'Login Successfully',
 1005 => 'Please Try Again',
 2001 => 'Email Required',
 2002 => 'Password Required',
 
 /*****************************************************************/
 
 API for signup
 URL:
 http://223.196.72.250/justalent/api/signup
 
 Variables Required:
 email, password, username, mobile, dialingCode
 
 Response Code:
 1004 => 'SignUp Successfull',
 1005 => 'Please Try Again',
 1007 => 'Something went wrong. Please try again',
 1009 => 'Email already exists',
 
 /*****************************************************************/
 
 */

